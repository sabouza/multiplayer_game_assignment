# NETWORKING ASSIGNMENT CGL #
Fourth Semester CGL Programming Assignment - Online Multiplayer Game :
This Application is an attempt/example of a working online soccer game between 2 players(on two different devices).

### Copyright ###
Sarah Abouzari Sorkheriz,
Bachelor Student at Cologne Game Lab 2015-2019

### Gameplay Goal: ###
Kick the ball into the goal of the other team and score points. This can go endless..
The Application is controlled with keyboard input (Mouse input does not work). You can control the player with the arrow keys and WSAD

### What is it and how do I start it? ###

This project exists out of the SFML-Library and is mainly based on C++. The networking classes itself are taken from the SFML-Library as well (Tutorials are taken from: https://www.sfml-dev.org/tutorials/2.4/).
To start the game, execute the following file cgl_networking_Assignment\x64\Debug\cgl_networking_assignment.exe.
If this doesn't work, Debug the project from the solution in Visual Studio..
* 127.0.0.1 is used as the default IP adress of the server, on port 5000 

### Short/Important Classes Overview ###

* States:
The Application exists out of different States: Title (The title screen on start), Menu (the main menu), Pause (the pause menu on "ESC"), MultiplayerState (as soon as both host and client are connected)...
The states are switched between eachother by calling them through simple methods. The Networking itself consists out of several States as well (successfull connection/Failed Connection etc.).
* Category:
The Category system allows us to differentiate between different objects. This is needed for the detection of objects and their collision in for example, if the player hits the playfield outline or the ball. The same behaviour counts for the ball itself as well.
* World:
This class manages all related functions to setup the gameworld/soccer field for the player. 
For example: The interaction of host and clients are checked, the world collisions...
The UI itself also originates from this class, where the team points and texts are being displayed.
* Networking(Host/Client):
The networking behaviour is referenced in each single class.
* GameServer:
All Interactions with requests of clients happen here. Synchronization of player and ball movements happen for example here. 
* MultiplayerGameState:
This class is used for managing the connection to the server, player input and events, ...