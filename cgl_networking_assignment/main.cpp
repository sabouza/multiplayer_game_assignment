#include <hppFiles/GameApp.hpp>

#include <stdexcept>
#include <iostream>

//Try to run the application
int main()
{
	try
	{
		GameApp app;
		app.run();
	}
	catch (std::exception& e)
	{
		std::cout << "An exception occurred." << e.what() << std::endl;
	}
}
