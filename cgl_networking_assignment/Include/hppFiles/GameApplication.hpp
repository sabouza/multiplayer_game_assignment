#include <hppFiles/ResourceHolder.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/KeyBinding.hpp>
#include <hppFiles/StateStack.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>


class GameApplication
{
public:
	GameApplication();
	void run();

private:
	void Render();
	void ProcessInput();
	void RegisterState();
	void Update(sf::Time dt);
	void UpdateStatistic(sf::Time dt);

	static const sf::Time	TimePerFrame;

	sf::RenderWindow		Window;
	TextureHolder			Textures;
	FontHolder				Fonts;
	KeyBinding				KeyLayout1;
	KeyBinding				KeyLayout2;
	StateStack				stateStack;
	sf::Text				StatisticText;
	sf::Time				StatisticTime;
	std::size_t				StatisticFPS;
};