#ifndef GamePlayerHpp
#define GamePlayerHpp

#include <hppFiles/Commands.hpp>
#include <hppFiles/KeyBinding.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Network/TcpSocket.hpp>

#include <map>


class CommandsQueue;

class Player : private sf::NonCopyable
{
public:
	typedef PlayerAction::Type Action;

	enum playerGameStatus
	{
		moving,
		won,
		losing
	};


public:
	Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding);
	bool isLocal() const;
	void ManageEvent(const sf::Event& event, CommandsQueue& commands);
	void ManageRealtimeInput(CommandsQueue& commands);
	void ManageRealtimeNetworkInput(CommandsQueue& commands);
	// React to events or realtime state changes received over the network
	void ManageNetworkEvent(Action action, CommandsQueue& commands);
	void ManageRealtimeNetworkChange(Action action, bool actionEnabled);
	playerGameStatus GetMissionStat() const;
	void SetMissionStat(playerGameStatus status);
	void DisableRealtimeActions();


private:
	int playerID;
	void ActionInit();
	sf::TcpSocket* socket;
	const KeyBinding* keyBinding;
	std::map<Action, Command> actionBinding;
	std::map<Action, bool> actionProxies;
	playerGameStatus currentGameStatus;
};

#endif // GamePlayerHpp
