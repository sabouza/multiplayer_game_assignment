#ifndef GameMenuStateHpp
#define GameMenuStateHpp

#include <hppFiles/State.hpp>
#include <hppFiles/GameContainer.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class MenuState : public State
{
public:
	MenuState(StateStack& stack, Context context);
	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);


private:
	GUI::Container guiContainer;
	sf::Sprite backgroundSprite;
};

#endif // GameMenuStateHpp
