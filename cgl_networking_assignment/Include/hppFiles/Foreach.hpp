#ifndef GameForeachHpp
#define GameForeachHpp

//Preprocessor enables us to allow nested loops
#define PreProCategoryImplementation(a, b) a ## b
#define PreProCategory(a, b) PreProCategoryImplementation(a, b)
#define GameID(identifier) PreProCategory(auroraDetail_, identifier)
#define GameLineID(identifier) PreProCategory(GameID(identifier), __LINE__)

#define FOREACH(declaration, container) \
	if (bool GameLineID(broken) = false) {} else \
	for (auto GameLineID(itr) = (container).begin(); GameLineID(itr) != (container).end() && !GameLineID(broken); ++GameLineID(itr)) \
	if (bool GameLineID(passed) = false) {} else \
	if (GameLineID(broken) = true, false) {} else \
	for (declaration = *GameLineID(itr); !GameLineID(passed); GameLineID(passed) = true, GameLineID(broken) = false)

#endif // GameForeachHpp
