//CONTAINS TEXT AND STATE BEHAVIOUR OF GAME OVER

#ifndef GameGameOverHpp
#define GameGameOverHpp

#include <hppFiles/State.hpp>
#include <hppFiles/GameContainer.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameOverState : public State
{
public:
	GameOverState(StateStack& stack, Context context, const std::string& text);
	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);

private:
	sf::Text gameOverText;
	sf::Time elapsedTime;
};

#endif // GameGameOverHpp
