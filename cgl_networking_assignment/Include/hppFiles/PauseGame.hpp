#ifndef GamePauseHpp
#define GamePauseHpp

#include <hppFiles/State.hpp>
#include <hppFiles/GameContainer.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class PauseGame : public State
{
public:
	PauseGame(StateStack& stack, Context context, bool letUpdatesThrough = false);
	~PauseGame();
	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);


private:
	sf::Sprite backgroundSprite;
	sf::Text mPausedText;
	GUI::Container guiContainer;
	bool mLetUpdatesThrough;
};

#endif // GamePauseHpp
