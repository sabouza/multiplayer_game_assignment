#ifndef GameStateStackHpp
#define GameStateStackHpp

#include <vector>
#include <utility>
#include <functional>
#include <map>
#include <hppFiles/State.hpp>
#include <hppFiles/StateIdentifiers.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>

namespace sf
{
	class Event;
	class RenderWindow;
}

class StateStack : private sf::NonCopyable
{
public:
	enum Action
	{
		Push,
		Pop,
		Clear,
	};


public:
	explicit StateStack(State::Context context);
	template <typename T>
	void registerState(States::ID stateID);
	template <typename T, typename Param1>
	void registerState(States::ID stateID, Param1 arg1);
	void Update(sf::Time dt);
	void draw();
	void ManageEvent(const sf::Event& event);
	void pushState(States::ID stateID);
	void popState();
	void clearStates();
	bool isEmpty() const;

private:
	State::Ptr createState(States::ID stateID);
	void ApplyIncomingChanges();
	struct IncomingChange
	{
		explicit IncomingChange(Action action, States::ID stateID = States::None);
		Action action;
		States::ID stateID;
	};
	std::vector<State::Ptr> stateStack;
	std::vector<IncomingChange> pendingList;

	std::map<States::ID, std::function<State::Ptr()>> factories;
	State::Context _context;
};


template <typename T>
void StateStack::registerState(States::ID stateID)
{
	factories[stateID] = [this]()
	{
		return State::Ptr(new T(*this, _context));
	};
}

template <typename T, typename Param1>
void StateStack::registerState(States::ID stateID, Param1 arg1)
{
	factories[stateID] = [this, arg1]()
	{
		return State::Ptr(new T(*this, _context, arg1));
	};
}

#endif // GameStateStackHpp
