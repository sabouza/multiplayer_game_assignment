#ifndef GameResourceHolderHpp
#define GameResourceHolderHpp

#include <map>
#include <string>
#include <memory>
#include <stdexcept>
#include <cassert>


template <typename Resource, typename Identifier>
class ResourceContainer
{
	public:
		void LoadResource(Identifier id, const std::string& filename);
		template <typename Parameter>
		void LoadResource(Identifier id, const std::string& filename, const Parameter& secondParam);
		Resource& get(Identifier id);
		const Resource& get(Identifier id) const;

	private:
		void InsertResource(Identifier id, std::unique_ptr<Resource> resource);
		std::map<Identifier, std::unique_ptr<Resource>>	mResourceMap;
};

#include "ResourceContainer.inl"
#endif // GameResourceHolderHpp
