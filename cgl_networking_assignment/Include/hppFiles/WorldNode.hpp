#ifndef GameWorldNodeHpp
#define GameWorldNodeHpp

#include <vector>
#include <set>
#include <memory>
#include <utility>
#include <hppFiles/Category.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>

struct Command;
class CommandsQueue;

class WorldNode : public sf::Transformable, public sf::Drawable, private sf::NonCopyable
{
public:
	typedef std::unique_ptr<WorldNode> Ptr;
	typedef std::pair<WorldNode*, WorldNode*> Pair;
	explicit WorldNode(Category::Type category = Category::None);
	void Update(sf::Time dt, CommandsQueue& commands);
	void AttachChild(Ptr child);
	void CheckSceneCollision(WorldNode& sceneGraph, std::set<Pair>& collisionPairs);
	void CheckNodeCollision(WorldNode& node, std::set<Pair>& collisionPairs);
	void OnCommand(const Command& command, sf::Time dt);
	void RemoveWreck();
	Ptr DetachChild(const WorldNode& node);
	sf::Transform getWorldTransform() const;
	sf::Vector2f getWorldPosition() const;
	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	virtual bool isMarkedForRemoval() const;
	virtual bool isDestroyed() const;

private:
	void UpdateChildren(sf::Time dt, CommandsQueue& commands);
	void DrawChildren(sf::RenderTarget& target, sf::RenderStates states) const;
	void DrawBoundingRect(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void UpdateCurrent(sf::Time dt, CommandsQueue& commands);

	std::vector<Ptr> childrenContainer;
	WorldNode* mParent;
	Category::Type mDefaultCategory;
};

bool collision(const WorldNode& lhs, const WorldNode& rhs);
float distance(const WorldNode& lhs, const WorldNode& rhs);

#endif // GameWorldNodeHpp
