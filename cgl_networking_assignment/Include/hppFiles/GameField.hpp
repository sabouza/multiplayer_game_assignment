#ifndef Soccer_WORLD_HPP
#define Soccer_WORLD_HPP

#include <hppFiles/ResourceContainer.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/WorldNode.hpp>
#include <hppFiles/SpriteNode.hpp>
#include <hppFiles/PlayerPawn.hpp>
#include <hppFiles/CommandsQueue.hpp>
#include <hppFiles/Commands.hpp>
#include <hppFiles/ProcessedTextures.hpp>
#include <hppFiles/NetworkProtocol.hpp>
#include <hppFiles/GameApp.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "Ball.hpp"

#include <array>
#include <queue>


// Forward declaration
namespace sf
{
	class RenderTarget;
}

class NetworkNode;

class GameField : private sf::NonCopyable
{
public:
	GameField(sf::RenderTarget& outputTarget, FontHolder& fonts, bool networked = false);
	void								Update(sf::Time dt);
	void						draw();

	bool								isHost;
	Ball*							soccerBallG;

	sf::FloatRect						getViewBounds() const;
	CommandsQueue&						getCommandQueue();
	PlayerPawn*							addPlayerPawn(int identifier);
	void								spawnCenterBall(bool isHost);
	void								removePlayerPawn(int identifier);
	void								setCurrentBattleFieldPosition(float lineY);
	void								setWorldHeight(float height);

	void								addEnemy(PlayerPawn::Type type, float relX, float relY);
	void								sortEnemies();

	bool 								hasAlivePlayer() const;
	bool 								hasPlayerReachedEnd() const;

	PlayerPawn*							getPlayerPawn(int identifier) const;
	sf::FloatRect						getSoccerfieldBounds() const;
	sf::Text							Player1ScoreText;
	sf::Text							Player2ScoreText;
	sf::Text							mStatusText;

	int									Player1ScoreInt = 0;
	int									Player2ScoreInt = 0;

private:
	void								loadTextures();
	void								LimitPlayerPosition();
	void								adaptPlayerVelocity();
	void								DetectCollision();

	void								buildScene();
	void								spawnEnemies();
	void								destroyEntitiesOutsideView();


private:
	enum Layer
	{
		Background,
		LowerAir,
		UpperAir,
		LayerCount
	};

	struct SpawnPoint
	{
		SpawnPoint(PlayerPawn::Type type, float x, float y)
			: type(type)
			, x(x)
			, y(y)
		{
		}

		PlayerPawn::Type type;
		float x;
		float y;
	};


private:

	sf::RenderTarget&					mTarget;
	sf::RenderTexture					mSceneTexture;
	sf::View							WorldView;
	TextureHolder						Textures;
	FontHolder&							Fonts;

	WorldNode							worldGraph;
	std::array<WorldNode*, LayerCount>	mSceneLayers;
	CommandsQueue						mCommandQueue;

	sf::FloatRect						mWorldBounds;
	sf::Vector2f						StartPosition;
	std::vector<PlayerPawn*>				mPlayerPawns;

	ProcessedTextures							processedTextures;

	bool								mNetworkedWorld;
	NetworkNode*						mNetworkNode;
	SpriteNode*							mFinishSprite;
};

#endif // Soccer_WORLD_HPP
