#ifndef GameMultiplayerStateHpp
#define GameMultiplayerStateHpp

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <SFML/Network/Packet.hpp>
#include <hppFiles/State.hpp>
#include <hppFiles/GameField.hpp>
#include <hppFiles/Player.hpp>
#include <hppFiles/GameServer.hpp>
#include <hppFiles/NetworkProtocol.hpp>


class MultiplayerState : public State
{
public:
	MultiplayerState(StateStack& stack, Context context, bool isHost);
	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);
	virtual void OnActivate();
	void DisableRealtimeActions();


private:
	void handlePacket(sf::Int32 packetType, sf::Packet& packet);
	void updateBroadcastMessage(sf::Time elapsedTime);
	typedef std::unique_ptr<Player> PlayerPtr;
	GameField world;
	sf::RenderWindow& Window;
	std::map<int, PlayerPtr> players;
	std::vector<sf::Int32> localPlayerID;
	sf::TcpSocket socket;
	std::unique_ptr<GameServer> server;
	std::vector<std::string> broadcasts;
	sf::Text broadcastText;
	sf::Text spawnPlayerText;
	sf::Text connectionText;
	sf::Clock failedConnectiontime;
	sf::Time broadcastedTime;
	sf::Time invitationTime;
	sf::Time clientTimeout;
	sf::Time timeSinceLastPacket;
	bool isConnected;
	bool activateState;
	bool hasFocus;
	bool isHost;
	bool gameStarted;
};

#endif // GameMultiplayerStateHpp
