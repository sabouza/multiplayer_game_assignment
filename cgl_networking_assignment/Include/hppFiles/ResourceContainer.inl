//FUNCTION EXPLANATION
//-1- Create and Load Resource
//-2- If resource has been loaded successful, insert the resource to map

template <typename Resource, typename Identifier>
template <typename Parameter>
void ResourceContainer<Resource, Identifier>::LoadResource(Identifier id, const std::string& filename, const Parameter& secondParam)
{
	std::unique_ptr<Resource> resource(new Resource());
	if (!resource->loadFromFile(filename, secondParam))
		throw std::runtime_error("ResourceContainer,LoadResource - Could not load: " + filename);
	InsertResource(id, std::move(resource));
}
template <typename Resource, typename Identifier>

void ResourceContainer<Resource, Identifier>::LoadResource(Identifier id, const std::string& fileSource)
{
	std::unique_ptr<Resource> resource(new Resource());
	if (!resource->loadFromFile(fileSource))
		throw std::runtime_error("ResourceContainer,LoadResource - Could not load: " + fileSource);
	InsertResource(id, std::move(resource));
}
template <typename Resource, typename Identifier>
Resource& ResourceContainer<Resource, Identifier>::get(Identifier id)
{
	auto found = mResourceMap.find(id);
	assert(found != mResourceMap.end());

	return *found->second;
}

template <typename Resource, typename Identifier>
const Resource& ResourceContainer<Resource, Identifier>::get(Identifier id) const
{
	auto found = mResourceMap.find(id);
	assert(found != mResourceMap.end());

	return *found->second;
}

template <typename Resource, typename Identifier>
void ResourceContainer<Resource, Identifier>::InsertResource(Identifier id, std::unique_ptr<Resource> resource) 
{
	// Insert and check success
	auto inserted = mResourceMap.insert(std::make_pair(id, std::move(resource)));
	assert(inserted.second);
}
