#ifndef GameStateIdentifierHpp
#define GameStateIdentifierHpp


namespace States
{
	enum ID
	{
		None,
		Title,
		Menu,
		Pause,
		NetworkPause,
		HostGame,
		JoinGame,
		Game,
		GameOver,
		won,
	};
}

#endif // GameStateIdentifierHpp
