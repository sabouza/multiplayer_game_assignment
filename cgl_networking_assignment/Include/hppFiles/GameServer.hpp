//CONTAINS THE TCP NETWORK SERVER

#ifndef GameServerHpp
#define GameServerHpp

#include <vector>
#include <memory>
#include <map>
#include <SFML/System/Sleep.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Thread.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Network/TcpListener.hpp>
#include <SFML/Network/TcpSocket.hpp>

class GameServer
{
public:
	explicit GameServer(sf::Vector2f soccerfieldSize);
	~GameServer();
	void NotifyPlayerEvent(sf::Int32 PlayerPawnIdentifier, sf::Int32 action);
	void NotifyPlayerSpawn(sf::Int32 PlayerPawnIdentifier);
	void NotifyPlayerRealtime(sf::Int32 PlayerPawnIdentifier, sf::Int32 action, bool actionEnabled);


private:
	//REFERENCING A LOCAL/BROADED GAME INSTANCE
	struct ServerRemote
	{
		ServerRemote();
		sf::TcpSocket socket;
		sf::Time lastPacketTime;
		std::vector<sf::Int32>	PlayerPawnIDS;
		bool isReady;
		bool isTimedOut;
	};

	//STORING STATE INFORMATION ABOUT THE CURRENT PLAYERPAWN
	struct InfoPlayerPawn
	{
		sf::Int32 hitAmount;
		sf::Vector2f position;
		std::map<sf::Int32, bool> actionsRealtime;
	};

	typedef std::unique_ptr<ServerRemote> PeerPtr;


private:
	void SetListener(bool enable);
	void ExecuteThread();
	void ServerTime();
	sf::Time currentTime() const;

	void ReceivePackets();
	void handleIncomingPacket(sf::Packet& packet, ServerRemote& receivingPeer, bool& detectedTimeout);

	void ReceiveConnections();
	void HandleDisconnections();

	void UpdateClientState();
	void UpdateWorldState(sf::TcpSocket& socket);
	void BroadcastMessage(const std::string& message);
	void DistributeMessage(sf::Packet& packet);


private:
	sf::Thread thread;
	sf::Clock clock;
	sf::TcpListener listener;
	sf::Time clientTimeoutDuration;
	bool listeningState;
	std::size_t PlayerConnectionLimit;
	std::size_t ConnectedPlayers;
	float worldHeight;
	sf::FloatRect soccerFieldRect;
	std::size_t playerPawnAmount;
	std::map<sf::Int32, InfoPlayerPawn>	playerPawnInformation;
	sf::Vector2f ballPosition;
	sf::Vector2f ballVelocity;
	std::vector<PeerPtr> _Peers;
	sf::Int32 PlayerPawnIDCounter;
	bool isWaitingEndThread;
	sf::Time passedSpawnTime;
	sf::Time nextSpawnTime;
};

#endif // GameServerHpp
