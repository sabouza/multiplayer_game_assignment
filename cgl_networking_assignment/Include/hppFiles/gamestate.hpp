#ifndef GameStatusHpp
#define GameStatusHpp

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <hppFiles/State.hpp>
#include <hppFiles/GameField.hpp>
#include <hppFiles/Player.hpp>

class GameState : public State
{
public:
	GameState(StateStack& stack, Context context);

	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);

private:
	GameField world;
	Player _Player;
};

#endif // GameStatusHpp
