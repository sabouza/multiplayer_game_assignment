#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>


class Animation : public sf::Drawable, public sf::Transformable
{
	public:
		Animation();
		explicit Animation(const sf::Texture& texture);	
		void Update(sf::Time dt);
		void restart();
		void setRepeating(bool flag);
		bool isRepeating() const;
		bool isFinished() const;

		void setTexture(const sf::Texture& texture);
		const sf::Texture* getTexture() const;

		void setDuration(sf::Time duration);
		sf::Time getDuration() const;

		void setNumFrames(std::size_t numFrames);
		std::size_t getNumFrames() const;

		void setFrameSize(sf::Vector2i frameSize);
		sf::Vector2i getFrameSize() const;

		sf::FloatRect getLocalBounds() const;
		sf::FloatRect getGlobalBounds() const;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		sf::Sprite Sprite;
		sf::Vector2i frameSize;
		std::size_t frameAmount;
		std::size_t activeFrame;
		sf::Time duration;
		sf::Time elapsedTime;
		bool repeat;
};