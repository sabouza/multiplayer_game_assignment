#ifndef GameNodeNetworkHpp
#define GameNodeNetworkHpp

#include <queue>
#include <hppFiles/NetworkProtocol.hpp>
#include <hppFiles/WorldNode.hpp>

class NetworkNode : public WorldNode
{
public:
	NetworkNode();
	virtual unsigned int getCategory() const;
};

#endif // GameNodeNetworkHpp
