//IMPLEMENTATION OF FUNCTIONS BASED ON ALL INFORMATION ON THE PLAYERS GIVEN VALUES (in datatables)
//--- Player Movement and hit behaviour

#ifndef Soccer_ENTITY_HPP
#define Soccer_ENTITY_HPP

#include <hppFiles/WorldNode.hpp>

class Entity : public WorldNode
{
	public:
		explicit Entity(int hitAmount);

		int	GetHitAmount() const;
		void SetHitAmount(int points);

		sf::Vector2f GetVelocity() const;
		void SetVelocity(sf::Vector2f velocity);
		void SetVelocity(float vx, float vy);
		void AccelerateVelocity(sf::Vector2f velocity);
		void AccelerateVelocity(float vx, float vy);

	protected:
		virtual void UpdateCurrent(sf::Time dt, CommandsQueue& commands);

	private:
		sf::Vector2f Velocity;
		int _hitAmount;
};

#endif // Soccer_ENTITY_HPP
