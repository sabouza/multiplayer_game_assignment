#ifndef GamePlayerPawnHpp
#define GamePlayerPawnHpp

#include <hppFiles/Entity.hpp>
#include <hppFiles/Commands.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/TextNode.hpp>
#include <hppFiles/Animation.hpp>
#include <SFML/Graphics/Sprite.hpp>

class PlayerPawn : public Entity
{
public:
	enum Type
	{
		Blue,
		Red,
		TypeCount
	};

public:
	PlayerPawn(Type type, const TextureHolder& textures, const FontHolder& fonts);
	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	virtual bool isMarkedForRemoval() const;
	bool isAllied() const;
	float getMaxSpeed() const;
	int	getIdentifier();
	void SetIdentifier(int identifier);

private:
	virtual void DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void UpdateCurrent(sf::Time dt, CommandsQueue& commands);
	void UpdateTexts();
	void UpdateAnimation();

private:
	Type type;
	sf::Sprite Sprite;
	int playerID;
};

#endif // GamePlayerPawnHpp
