#ifndef GameContainerHpp
#define GameContainerHpp

#include <vector>
#include <memory>
#include <hppFiles/Component.hpp>

namespace GUI
{
	class Container : public Component
	{
	public:
		typedef std::shared_ptr<Container> Ptr;

	public:
		Container();
		void pack(Component::Ptr component);
		virtual bool isSelectable() const;
		virtual void ManageEvent(const sf::Event& event);

	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		bool hasSelection() const;
		void Select(std::size_t index);
		void SelectNextComponent();
		void SelectPreviousComponent();

	private:
		std::vector<Component::Ptr> childrenContainer;
		int	selectedChild;
	};
}

#endif // GameContainerHpp
