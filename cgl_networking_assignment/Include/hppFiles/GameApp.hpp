#ifndef Soccer_APPLICATION_HPP
#define Soccer_APPLICATION_HPP

#include <hppFiles/ResourceContainer.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/KeyBinding.hpp>
#include <hppFiles/StateStack.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>


class GameApp
{
	public:
		GameApp();
		void run();
		

	private:
		void ProcessInput();
		void Update(sf::Time dt);
		void Render();
		void UpdateStatistic(sf::Time dt);
		void RegisterState();

		static const sf::Time TimePerFrame;

		sf::RenderWindow Window;
		TextureHolder Textures;
	  	FontHolder Fonts;
		KeyBinding keyBinding1;
		KeyBinding keyBinding2;
		StateStack stateStack;
		sf::Text StatisticText;
		sf::Time StatisticTime;
		std::size_t	StatisticFPS;
};

#endif // Soccer_APPLICATION_HPP
