#ifndef GameResourceIDHpp
#define GameResourceIDHpp

//DECLARATION OF SFML CLASSES
namespace sf
{
	class Texture;
	class Font;
	class Shader;
	class SoundBuffer;
}

//IDENTIFIERS TEXTURE RESOURCES
namespace Textures
{
	enum ID
	{
		Player1,
		Player2,
		Entities,
		Ball,
		Background,
		MenuBackground,
		Buttons
	};
}
//IDENTIFIERT OF SHADER TO RENDER
namespace Shaders
{
	enum ID
	{
		AddPass,
	};
}
//IDENTIFIER OF FONT RESOURCE
namespace Fonts
{
	enum ID
	{
		Bebas,
	};
}



//Declaration and type definitions
template <typename Resource, typename Identifier>
class ResourceContainer;

typedef ResourceContainer<sf::Texture, Textures::ID>			TextureHolder;
typedef ResourceContainer<sf::Font, Fonts::ID>					FontHolder;
typedef ResourceContainer<sf::Shader, Shaders::ID>				ShaderHolder;

#endif // GameResourceIDHpp
