#ifndef GameNetworkProtocolHpp
#define GameNetworkProtocolHpp

#include <SFML/Config.hpp>
#include <SFML/System/Vector2.hpp>


const unsigned short ServerPort = 5000;

// --- PACKET TYPES FROM THE SERVER
namespace Server
{
	enum PacketType
	{
		BroadcastMessage,
		SpawnSelf,
		InitialState,
		PlayerEvent,
		PlayerRealtimeChange,
		PlayerConnect,
		PlayerDisconnect,
		AcceptCoopPartner,
		UpdateClientState,
		won
	};
}

//--- PACKET TYPES FROM THE CLIENT
namespace Client
{
	enum PacketType
	{
		PlayerEvent,
		PlayerRealtimeChange,
		RequestCoopPartner,
		PositionUpdate,
		GameEvent,
		Quit
	};
}

#endif // GameNetworkProtocolHpp
