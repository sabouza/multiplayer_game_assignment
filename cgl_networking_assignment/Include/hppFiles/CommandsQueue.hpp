#ifndef GameCommandQueueHpp
#define GameCommandQueueHpp
#include <hppFiles/Commands.hpp>
#include <queue>

class CommandsQueue
{
	public:
		void push(const Command& command);
		Command	pop();
		bool isEmpty() const;	
	private:
		std::queue<Command> mQueue;
};

#endif // GameCommandQueueHpp
