#ifndef GameTextNodeHpp
#define GameTextNodeHpp

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/WorldNode.hpp>

class TextNode : public WorldNode
{
	public:
		explicit TextNode(const FontHolder& fonts, const std::string& text);
		void setString(const std::string& text);

	private:
		virtual void DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
		sf::Text Text;
};

#endif // GameTextNodeHpp
