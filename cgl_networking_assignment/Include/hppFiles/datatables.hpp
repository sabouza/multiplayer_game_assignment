//CONTAINS ALL BASE PLAYER DATA
//--- speed, hitamount, texture, animation, movement..

#ifndef GameDatatablesHpp
#define GameDatatablesHpp

#include <vector>
#include <functional>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>

class Player;

struct MoveDirection
{
	MoveDirection(float angle, float distance)
		: angle(angle)
		, distance(distance)
	{
	}
	float angle;
	float distance;
};

struct PlayerPawnData
{
	float speed;
	int	hitAmount;
	bool hasAnimation;
	Textures::ID texture;
	sf::IntRect textureRect;
	std::vector<MoveDirection> moveDirections;
};

std::vector<PlayerPawnData>	InitPlayerPawnData();

#endif // GameDatatablesHpp
