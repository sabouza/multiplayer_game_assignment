//--- CONTAINER OF SCENE NODE CATEGORIES FOR DISPATCHING COMMANDS

#ifndef GameCategoryHpp
#define GameCategoryHpp

namespace Category
{
	enum Type
	{
		None = 0,
		SceneAirLayer = 1 << 0,
		Player = 1 << 1,
		AlliedPawn = 1 << 2,
		EnemyPawn = 1 << 3,
		Ball = 1 << 4,
		Network				= 1 << 9,
		Pawn = Player | AlliedPawn | EnemyPawn,
	};
}

#endif // GameCategoryHpp
