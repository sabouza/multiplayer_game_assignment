//--- BUTTON SETTINGS set: 
//--- Selection state
//--- select/deselect Texture
//--- Event receiver
//--- menu button positionings
//--- etc.

#ifndef GameButtonHpp
#define GameButtonHpp

#include <vector>
#include <string>
#include <memory>
#include <functional>
#include <hppFiles/Component.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/State.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

namespace GUI
{

class MenuButtons : public Component
{
    public:
        typedef std::shared_ptr<MenuButtons> Ptr;
        typedef std::function<void()> Callback;

		//MenuButtons States
		enum Type
		{
			deselected,
			selected,
		};

	public:
		MenuButtons(State::Context context);
        void SetCallback(Callback callback);
		void SetText(const std::string& text);
        void SetToggle(bool flag);
        virtual bool isSelectable() const;
        virtual void Select();
        virtual void Deselect();
        virtual void Activate();
        virtual void Deactivate();
        virtual void ManageEvent(const sf::Event& event);


    private:
        bool isToggle;
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		void SetTexture(Type buttonType);
        Callback buttonCallback;
        sf::Sprite Sprite;
        sf::Text Text;
};
}

#endif // GameButtonHpp
