#ifndef GameStateHpp
#define GameStateHpp

#include <memory>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>
#include <hppFiles/StateIdentifiers.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>


namespace sf
{
	class RenderWindow;
}

class StateStack;
class KeyBinding;

class State
{
public:
	typedef std::unique_ptr<State> Ptr;

	struct Context
	{
		sf::RenderWindow* window;
		KeyBinding* keys1;
		KeyBinding* keys2;
		FontHolder* fonts;
		TextureHolder* textures;
		Context(sf::RenderWindow& window, TextureHolder& textures, FontHolder& fonts, KeyBinding& keys1, KeyBinding& keys2);
	};

	State(StateStack& stack, Context context);
	virtual ~State();
	virtual void draw() = 0;
	virtual bool Update(sf::Time dt) = 0;
	virtual bool ManageEvent(const sf::Event& event) = 0;
	virtual void OnActivate();
	virtual void OnDestroy();

protected:
	void RequestPushStack(States::ID stateID);
	void RequestPopState();
	void RequestClearState();
	Context getContext() const;

private:
	StateStack* stateStack;
	Context _context;
};

#endif // GameStateHpp
