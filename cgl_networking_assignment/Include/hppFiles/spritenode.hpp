#ifndef GameSpriteNodeHpp
#define GameSpriteNodeHpp

#include <SFML/Graphics/Sprite.hpp>
#include <hppFiles/WorldNode.hpp>

class SpriteNode : public WorldNode
{
public:
	explicit SpriteNode(const sf::Texture& texture);
	SpriteNode(const sf::Texture& texture, const sf::IntRect& textureRect);

private:
	virtual void DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	sf::Sprite Sprite;
};

#endif // GameSpriteNodeHpp
