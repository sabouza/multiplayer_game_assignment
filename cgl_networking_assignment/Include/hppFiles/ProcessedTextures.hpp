#ifndef GameProcessedTexturesHpp
#define GameProcessedTexturesHpp
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <array>

namespace sf
{
	class RenderTarget;
	class RenderTexture;
	class Shader;
}

class ProcessedTextures : sf::NonCopyable
{
	public:
		ProcessedTextures();
		virtual void ApplyTexture(const sf::RenderTexture& input, sf::RenderTarget& output);


	private:
		typedef std::array<sf::RenderTexture, 2> RenderTextureArray;
		void PrepareTexture(sf::Vector2u size);
		void AddToTexture(const sf::RenderTexture& source, const sf::RenderTexture& effect, sf::RenderTarget& target);
		void DrawTextures(const sf::Shader& shader, sf::RenderTarget& output);
		ShaderHolder		shaders;
		sf::RenderTexture	RenderTexture;
		RenderTextureArray	primaryTextures;
		RenderTextureArray	secondaryTextures;
};

#endif // GameProcessedTexturesHpp
