#ifndef GameCommandsHpp
#define GameCommandsHpp

#include <cassert>
#include <functional>
#include <hppFiles/Category.hpp>
#include <SFML/System/Time.hpp>

class WorldNode;

struct Command
{
	typedef std::function<void(WorldNode&, sf::Time)> Action;
	Command();
	Action action;
	unsigned int category;
};

template <typename GameObject, typename Function>
//Cast will be checked if its safe first, then a function is invoked on its node
Command::Action derivedAction(Function fn)
{
	return [=] (WorldNode& node, sf::Time dt)
	{
		assert(dynamic_cast<GameObject*>(&node) != nullptr);
		fn(static_cast<GameObject&>(node), dt);
	};
}

#endif // GameCommandsHpp
