//DEFINITON OF KEY INPUT NAMING FOR THE PLAYER MOVEMENT

#ifndef GameKeybindingHpp
#define GameKeybindingHpp

#include <map>
#include <vector>
#include <SFML/Window/Keyboard.hpp>

namespace PlayerAction
{
	enum Type
	{
		moveLeft,
		moveRight,
		moveUp,
		moveDown,
		count
	};
}

class KeyBinding
{
public:
	typedef PlayerAction::Type Action;

public:
	std::vector<Action>	ReceiveRealtimeAction() const;

	explicit KeyBinding(int controlPreconfiguration);
	sf::Keyboard::Key GetAssignedKeycode(Action action) const;
	void AssignKeycodes(Action action, sf::Keyboard::Key key);
	bool checkAction(sf::Keyboard::Key key, Action& out) const;

private:
	void ActionInit();
	std::map<sf::Keyboard::Key, Action>	KeyCode;
};

bool isRealtimeAction(PlayerAction::Type action);

#endif // GameKeybindingHpp
