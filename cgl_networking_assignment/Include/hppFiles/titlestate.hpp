#ifndef GameTitleStateHpp
#define GameTitleStateHpp

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <hppFiles/State.hpp>


class TitleState : public State
{
public:
	TitleState(StateStack& stack, Context context);
	virtual void draw();
	virtual bool Update(sf::Time dt);
	virtual bool ManageEvent(const sf::Event& event);


private:
	sf::Sprite backgroundSprite;
	sf::Text Text;
	bool showText;
	sf::Time textBlinkTime;
};

#endif // GameTitleStateHpp
