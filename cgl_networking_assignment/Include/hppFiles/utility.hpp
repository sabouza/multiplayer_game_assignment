#ifndef GameUtilityHpp
#define GameUtilityHpp

#include <sstream>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/Vector2.hpp>

namespace sf
{
	class Sprite;
	class Text;
}

class Animation;
template <typename T>
std::string toString(const T& value);

// Convert enumerators to strings
std::string toString(sf::Keyboard::Key key);

// Call setOrigin() with the center of the object
void CenterObjectOrigin(sf::Sprite& sprite);
void CenterObjectOrigin(sf::Text& text);
void CenterObjectOrigin(Animation& animation);

// Degree/radian conversion
float toDegree(float radian);
float toRadian(float degree);

// Random number generation
int randomInt(int exclusiveMax);

// Vector operations
float length(sf::Vector2f vector);
sf::Vector2f unitVector(sf::Vector2f vector);

#include <hppFiles/Utility.inl>
#endif // GameUtilityHpp
