#include <hppFiles/GameServer.hpp>
#include <hppFiles/NetworkProtocol.hpp>
#include <hppFiles/Foreach.hpp>
#include <hppFiles/Utility.hpp>

#include <hppFiles/PlayerPawn.hpp>

#include <SFML/Network/Packet.hpp>

GameServer::ServerRemote::ServerRemote() 
: isReady(false)
, isTimedOut(false)
{
	socket.setBlocking(false);
}

GameServer::GameServer(sf::Vector2f soccerfieldSize)
: thread(&GameServer::ExecuteThread, this)
, listeningState(false)
, clientTimeoutDuration(sf::seconds(3.f))
, PlayerConnectionLimit(10)
, ConnectedPlayers(0)
, worldHeight(5000.f)
, soccerFieldRect(0.f, worldHeight - soccerfieldSize.y, soccerfieldSize.x, soccerfieldSize.y)
, playerPawnAmount(0)
, _Peers(1)
, PlayerPawnIDCounter(1)
, isWaitingEndThread(false)
, passedSpawnTime(sf::Time::Zero)
, nextSpawnTime(sf::seconds(5.f))
{
	listener.setBlocking(false);
	_Peers[0].reset(new ServerRemote());
	thread.launch();
}

GameServer::~GameServer()
{
	isWaitingEndThread = true;
	thread.wait();
}

void GameServer::NotifyPlayerRealtime(sf::Int32 PlayerPawnIdentifier, sf::Int32 action, bool actionEnabled)
{
	for (std::size_t i = 0; i < ConnectedPlayers; ++i)
	{
		if (_Peers[i]->isReady)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerRealtimeChange);
			packet << PlayerPawnIdentifier;
			packet << action;
			packet << actionEnabled;

			_Peers[i]->socket.send(packet);
		}
	}
}

void GameServer::NotifyPlayerEvent(sf::Int32 PlayerPawnIdentifier, sf::Int32 action)
{
	for (std::size_t i = 0; i < ConnectedPlayers; ++i)
	{
		if (_Peers[i]->isReady)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerEvent);
			packet << PlayerPawnIdentifier;
			packet << action;

			_Peers[i]->socket.send(packet);
		}
	}
}

void GameServer::NotifyPlayerSpawn(sf::Int32 PlayerPawnIdentifier)
{
	for (std::size_t i = 0; i < ConnectedPlayers; ++i)
	{
		if (_Peers[i]->isReady)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerConnect);
			packet << PlayerPawnIdentifier << playerPawnInformation[PlayerPawnIdentifier].position.x << playerPawnInformation[PlayerPawnIdentifier].position.y;
			_Peers[i]->socket.send(packet);
		}
	}
}

void GameServer::SetListener(bool enable)
{
	// Check if it isn't already listening
	if (enable)
	{	
		if (!listeningState)
			listeningState = (listener.listen(ServerPort) == sf::TcpListener::Done);
	}
	else
	{
		listener.close();
		listeningState = false;
	}
}

void GameServer::ExecuteThread()
{
	SetListener(true);

	sf::Time stepInterval = sf::seconds(1.f / 60.f);
	sf::Time stepTime = sf::Time::Zero;
	sf::Time tickInterval = sf::seconds(1.f / 20.f);
	sf::Time tickTime = sf::Time::Zero;
	sf::Clock stepClock, tickClock;

	while (!isWaitingEndThread)
	{	
		ReceivePackets();
		ReceiveConnections();

		stepTime += stepClock.getElapsedTime();
		stepClock.restart();

		tickTime += tickClock.getElapsedTime();
		tickClock.restart();


		// Fixed ServerTime step
		while (tickTime >= tickInterval)
		{
			ServerTime();
			tickTime -= tickInterval;
		}

		// Sleep to prevent server from consuming 100% CPU
		sf::sleep(sf::milliseconds(100));
	}	
}

void GameServer::ServerTime()
{
	UpdateClientState();

	// Check for round win = all soccers with position.y < offset
	bool allPlayerPawnsDone = true;
	FOREACH(auto pair, playerPawnInformation)
	{
		// As long as one player has not crossed the finish line yet, set variable to false
		if (pair.second.position.y > 0.f)
			allPlayerPawnsDone = false;
	}
	if (allPlayerPawnsDone)
	{
		sf::Packet missionSuccessPacket;
		missionSuccessPacket << static_cast<sf::Int32>(Server::won);
		DistributeMessage(missionSuccessPacket);
	}

	// Remove IDs of Player that have been destroyed (relevant if a client has two, and loses one)
	for (auto itr = playerPawnInformation.begin(); itr != playerPawnInformation.end(); )
	{
		if (itr->second.hitAmount <= 0)
			playerPawnInformation.erase(itr++);
		else
			++itr;
	}
}

sf::Time GameServer::currentTime() const
{
	return clock.getElapsedTime();
}

void GameServer::ReceivePackets()
{
	bool detectedTimeout = false;
	
	FOREACH(PeerPtr& peer, _Peers)
	{
		if (peer->isReady)
		{
			sf::Packet packet;
			while (peer->socket.receive(packet) == sf::Socket::Done)
			{
				// Interpret packet and react to it
				handleIncomingPacket(packet, *peer, detectedTimeout);

				// Packet was indeed received, Update the ping timer
				peer->lastPacketTime = currentTime();
				packet.clear();
			}

			if (currentTime() >= peer->lastPacketTime + clientTimeoutDuration)
			{
				peer->isTimedOut = true;
				detectedTimeout = true;
			}
		}
	}

	if (detectedTimeout)
		HandleDisconnections();
}

void GameServer::handleIncomingPacket(sf::Packet& packet, ServerRemote& receivingPeer, bool& detectedTimeout)
{
	sf::Int32 packetType;
	packet >> packetType;

	switch (packetType)
	{
		case Client::Quit:
		{
			receivingPeer.isTimedOut = true;
			detectedTimeout = true;
		} break;

		case Client::PlayerEvent:
		{
			sf::Int32 PlayerPawnIdentifier;
			sf::Int32 action;
			packet >> PlayerPawnIdentifier >> action;

			NotifyPlayerEvent(PlayerPawnIdentifier, action);
		} break;

		case Client::PlayerRealtimeChange:
		{
			sf::Int32 PlayerPawnIdentifier;
			sf::Int32 action;
			bool actionEnabled;
			packet >> PlayerPawnIdentifier >> action >> actionEnabled;
			playerPawnInformation[PlayerPawnIdentifier].actionsRealtime[action] = actionEnabled;
			NotifyPlayerRealtime(PlayerPawnIdentifier, action, actionEnabled);
		} break;

		case Client::RequestCoopPartner:
		{
			receivingPeer.PlayerPawnIDS.push_back(PlayerPawnIDCounter);
			playerPawnInformation[PlayerPawnIDCounter].position = sf::Vector2f(soccerFieldRect.width / 2, soccerFieldRect.top + soccerFieldRect.height / 2);
			playerPawnInformation[PlayerPawnIDCounter].hitAmount = 100;

			sf::Packet requestPacket;
			requestPacket << static_cast<sf::Int32>(Server::AcceptCoopPartner);
			requestPacket << PlayerPawnIDCounter;
			requestPacket << playerPawnInformation[PlayerPawnIDCounter].position.x;
			requestPacket << playerPawnInformation[PlayerPawnIDCounter].position.y;

			receivingPeer.socket.send(requestPacket);
			playerPawnAmount++;

			// Inform every other peer 
			FOREACH(PeerPtr& peer, _Peers)
			{
				if (peer.get() != &receivingPeer && peer->isReady)
				{
					sf::Packet notifyPacket;
					notifyPacket << static_cast<sf::Int32>(Server::PlayerConnect);
					notifyPacket << PlayerPawnIDCounter;
					notifyPacket << playerPawnInformation[PlayerPawnIDCounter].position.x;
					notifyPacket << playerPawnInformation[PlayerPawnIDCounter].position.y;
					peer->socket.send(notifyPacket);
				}
			}
			PlayerPawnIDCounter++;
		} break;

		case Client::PositionUpdate:
		{
			sf::Int32 numPlayerPawns;
			packet >> numPlayerPawns;

			for (sf::Int32 i = 0; i < numPlayerPawns; ++i)
			{
				sf::Int32 PlayerPawnIdentifier;
				sf::Vector2f PlayerPawnPosition;
				sf::Vector2f SoccerBallPosition;
				sf::Vector2f SoccerBallVelocity;
				packet >> PlayerPawnIdentifier >> PlayerPawnPosition.x >> PlayerPawnPosition.y >> SoccerBallPosition.x >> SoccerBallPosition.y << SoccerBallVelocity.x >> SoccerBallVelocity.y;
				playerPawnInformation[PlayerPawnIdentifier].position = PlayerPawnPosition;
				
				if (PlayerPawnIdentifier == 1) {
					ballPosition = SoccerBallPosition;
					ballVelocity = SoccerBallVelocity;
				}


			}
		} break;

		case Client::GameEvent:
		{
			sf::Int32 action;
			float x;
			float y;

			packet >> action;
			packet >> x;
			packet >> y;
		}
	}
}

void GameServer::UpdateClientState()
{
	sf::Packet updateClientStatePacket;
	updateClientStatePacket << static_cast<sf::Int32>(Server::UpdateClientState);
	updateClientStatePacket << ballPosition.x << ballPosition.y << ballVelocity.x << ballVelocity.y;
	updateClientStatePacket << static_cast<sf::Int32>(playerPawnInformation.size());

	FOREACH(auto Player, playerPawnInformation)
		updateClientStatePacket << Player.first << Player.second.position.x << Player.second.position.y;

	DistributeMessage(updateClientStatePacket);
}

void GameServer::ReceiveConnections()
{
	if (!listeningState)
		return;

	if (listener.accept(_Peers[ConnectedPlayers]->socket) == sf::TcpListener::Done)
	{
		// order the new client to spawn its own soccer pawn ( player 1 )
		playerPawnInformation[PlayerPawnIDCounter].position = sf::Vector2f(soccerFieldRect.width / 2, soccerFieldRect.top + soccerFieldRect.height / 2);
		playerPawnInformation[PlayerPawnIDCounter].hitAmount = 100;

		sf::Packet packet;
		packet << static_cast<sf::Int32>(Server::SpawnSelf);
		packet << PlayerPawnIDCounter;
		packet << playerPawnInformation[PlayerPawnIDCounter].position.x;
		packet << playerPawnInformation[PlayerPawnIDCounter].position.y;
		
		_Peers[ConnectedPlayers]->PlayerPawnIDS.push_back(PlayerPawnIDCounter);
		
		BroadcastMessage("New player!");
		UpdateWorldState(_Peers[ConnectedPlayers]->socket);
		NotifyPlayerSpawn(PlayerPawnIDCounter++);

		_Peers[ConnectedPlayers]->socket.send(packet);
		_Peers[ConnectedPlayers]->isReady = true;
		_Peers[ConnectedPlayers]->lastPacketTime = currentTime(); // prevent initial timeouts
		playerPawnAmount++;
		ConnectedPlayers++;

		if (ConnectedPlayers >= PlayerConnectionLimit)
			SetListener(false);
		else // Add a new waiting peer
			_Peers.push_back(PeerPtr(new ServerRemote()));
	}
}

void GameServer::HandleDisconnections()
{
	for (auto itr = _Peers.begin(); itr != _Peers.end(); )
	{
		if ((*itr)->isTimedOut)
		{
			// Inform everyone of the disconnection, erase 
			FOREACH(sf::Int32 identifier, (*itr)->PlayerPawnIDS)
			{
				DistributeMessage(sf::Packet() << static_cast<sf::Int32>(Server::PlayerDisconnect) << identifier);

				playerPawnInformation.erase(identifier);
			}

			ConnectedPlayers--;
			playerPawnAmount -= (*itr)->PlayerPawnIDS.size();

			itr = _Peers.erase(itr);

			// Go back to a listening state if needed
			if (ConnectedPlayers < PlayerConnectionLimit)
			{
				_Peers.push_back(PeerPtr(new ServerRemote()));
				SetListener(true);
			}
				
			BroadcastMessage("An ally has disconnected.");
		}
		else
		{
			++itr;
		}
	}
}

// Tell the newly connected peer about how the world is currently
void GameServer::UpdateWorldState(sf::TcpSocket& socket)
{
	sf::Packet packet;
	packet << static_cast<sf::Int32>(Server::InitialState);
	packet << worldHeight << soccerFieldRect.top + soccerFieldRect.height;
	packet << static_cast<sf::Int32>(playerPawnAmount);

	for (std::size_t i = 0; i < ConnectedPlayers; ++i)
	{
		if (_Peers[i]->isReady)
		{
			FOREACH(sf::Int32 identifier, _Peers[i]->PlayerPawnIDS)
				packet << identifier << playerPawnInformation[identifier].position.x << playerPawnInformation[identifier].position.y << ballPosition.x << ballPosition.y << ballVelocity.x << ballVelocity.y;
		}
	}
	
	socket.send(packet);
}

void GameServer::BroadcastMessage(const std::string& message)
{
	for (std::size_t i = 0; i < ConnectedPlayers; ++i)
	{
		if (_Peers[i]->isReady)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::BroadcastMessage);
			packet << message;

			_Peers[i]->socket.send(packet);
		}	
	}
}

void GameServer::DistributeMessage(sf::Packet& packet)
{
	FOREACH(PeerPtr& peer, _Peers)
	{
		if (peer->isReady)
			peer->socket.send(packet);
	}
}
