#include <hppFiles/KeyBinding.hpp>
#include <hppFiles/Foreach.hpp>

#include <string>
#include <algorithm>


KeyBinding::KeyBinding(int controlPreconfiguration)
: KeyCode()
{
	// Set initial key bindings for player 1
	if (controlPreconfiguration == 1)
	{
		KeyCode[sf::Keyboard::Down]  = PlayerAction::moveDown;
		KeyCode[sf::Keyboard::Up]    = PlayerAction::moveUp;
		KeyCode[sf::Keyboard::Left]	 = PlayerAction::moveLeft;
		KeyCode[sf::Keyboard::Right] = PlayerAction::moveRight;
	}
	else if (controlPreconfiguration == 2)
	{
		// Player 2
		KeyCode[sf::Keyboard::W] = PlayerAction::moveUp;
		KeyCode[sf::Keyboard::S] = PlayerAction::moveDown;
		KeyCode[sf::Keyboard::A] = PlayerAction::moveLeft;
		KeyCode[sf::Keyboard::D] = PlayerAction::moveRight;
	}
}

void KeyBinding::AssignKeycodes(Action action, sf::Keyboard::Key key)
{
	// Remove all keys that already map to action
	for (auto itr = KeyCode.begin(); itr != KeyCode.end(); )
	{
		if (itr->second == action)
			KeyCode.erase(itr++);
		else
			++itr;
	}

	// Insert new binding
	KeyCode[key] = action;
}

sf::Keyboard::Key KeyBinding::GetAssignedKeycode(Action action) const
{
	FOREACH(auto pair, KeyCode)
	{
		if (pair.second == action)
			return pair.first;
	}

	return sf::Keyboard::Unknown;
}

bool KeyBinding::checkAction(sf::Keyboard::Key key, Action& out) const
{
	auto found = KeyCode.find(key);
	if (found == KeyCode.end())
	{
		return false;
	}
	else
	{
		out = found->second;
		return true;
	}
}

std::vector<KeyBinding::Action> KeyBinding::ReceiveRealtimeAction() const
{
	// Return all realtime actions that are currently active.
	std::vector<Action> actions;

	FOREACH(auto pair, KeyCode)
	{
		// If key is pressed and an action is a realtime action, store it
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
			actions.push_back(pair.second);
	}

	return actions;
}

bool isRealtimeAction(PlayerAction::Type action)
{
	switch (action)
	{
		case PlayerAction::moveLeft:
		case PlayerAction::moveRight:
		case PlayerAction::moveDown:
		case PlayerAction::moveUp:
			return true;

		default:
			return false;
	}
}