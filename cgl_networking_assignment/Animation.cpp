#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <hppFiles/Animation.hpp>


Animation::Animation()
	: Sprite()
	, activeFrame(0)
	, frameAmount(0)
	, frameSize()
	, duration(sf::Time::Zero)
	, elapsedTime(sf::Time::Zero)
	, repeat(false)
{
}
Animation::Animation(const sf::Texture& texture)
	: Sprite(texture)
	, activeFrame(0)
	, frameAmount(0)
	, frameSize()
	, duration(sf::Time::Zero)
	, elapsedTime(sf::Time::Zero)
	, repeat(false)
{
}

const sf::Texture* Animation::getTexture() const
{
	return Sprite.getTexture();
}
void Animation::setTexture(const sf::Texture& texture)
{
	Sprite.setTexture(texture);
}
sf::Vector2i Animation::getFrameSize() const
{
	return frameSize;
}
void Animation::setFrameSize(sf::Vector2i frameSize)
{
	frameSize = frameSize;
}
void Animation::setNumFrames(std::size_t numFrames)
{
	frameAmount = numFrames;
}
std::size_t Animation::getNumFrames() const
{
	return frameAmount;
}
sf::Time Animation::getDuration() const
{
	return duration;
}
void Animation::setDuration(sf::Time duration)
{
	duration = duration;
}
void Animation::setRepeating(bool flag)
{
	repeat = flag;
}
bool Animation::isRepeating() const
{
	return repeat;
}
void Animation::restart()
{
	activeFrame = 0;
}
bool Animation::isFinished() const
{
	return activeFrame >= frameAmount;
}
sf::FloatRect Animation::getLocalBounds() const
{
	return sf::FloatRect(getOrigin(), static_cast<sf::Vector2f>(getFrameSize()));
}
sf::FloatRect Animation::getGlobalBounds() const
{
	return getTransform().transformRect(getLocalBounds());
}

void Animation::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(Sprite, states);
}

/*While the frame is processing, move the texture to the left.
If the end of the texture has been reached, move it down a line and continue with processig the next frame*/
void Animation::Update(sf::Time dt)
{
	sf::Time timePerFrame = duration / static_cast<float>(frameAmount);
	elapsedTime += dt;

	sf::Vector2i textureBounds(Sprite.getTexture()->getSize());
	sf::IntRect textureRect = Sprite.getTextureRect();

	if (activeFrame == 0)
		textureRect = sf::IntRect(0, 0, frameSize.x, frameSize.y);

	while (elapsedTime >= timePerFrame && (activeFrame <= frameAmount || repeat))
	{
		textureRect.left += textureRect.width;
		if (textureRect.left + textureRect.width > textureBounds.x)
		{
			textureRect.left = 0;
			textureRect.top += textureRect.height;
		}

		elapsedTime -= timePerFrame;
		if (repeat)
		{
			activeFrame = (activeFrame + 1) % frameAmount;
			if (activeFrame == 0)
				textureRect = sf::IntRect(0, 0, frameSize.x, frameSize.y);
		}
		else
			activeFrame++;
	}
	Sprite.setTextureRect(textureRect);
}