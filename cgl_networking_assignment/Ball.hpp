
#include <hppFiles/Entity.hpp>
#include <hppFiles/Commands.hpp>
#include <hppFiles/ResourceIdentifiers.hpp>
#include <hppFiles/PlayerPawn.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

class PlayerPawn;

class Ball : public Entity
{

public:

	Ball(const TextureHolder& textures, bool isHost);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;
	void					BounceOffVerticalLines();
	void					BounceOffHorizontalLines();
	sf::Time                dt; // delta time
	sf::Time                elapsedTime;
	sf::Clock				clock;
	void					reset();
	bool					IsHost;
private:
	virtual void			DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void 			UpdateCurrent(sf::Time dt, CommandsQueue& commands);
	sf::Sprite				Sprite;

};
