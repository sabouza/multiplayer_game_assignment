#include <hppFiles/GameOverState.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/Player.hpp>
#include <hppFiles/ResourceContainer.hpp>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


GameOverState::GameOverState(StateStack& stack, Context context, const std::string& text)
: State(stack, context)
, gameOverText()
, elapsedTime(sf::Time::Zero)
{
	sf::Font& font = context.fonts->get(Fonts::Bebas);
	sf::Vector2f windowSize(context.window->getSize());
	
	gameOverText.setFont(font);
	gameOverText.setString(text);
	gameOverText.setCharacterSize(70);
	CenterObjectOrigin(gameOverText);
	gameOverText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);
}

void GameOverState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	// Create dark, semitransparent background
	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(gameOverText);
}

bool GameOverState::Update(sf::Time dt)
{
	// Show state for 3 seconds, after return to menu
	elapsedTime += dt;
	if (elapsedTime > sf::seconds(3))
	{
		RequestClearState();
		RequestPushStack(States::Menu);
	}
	return false;
}

bool GameOverState::ManageEvent(const sf::Event&)
{
	return false;
}
