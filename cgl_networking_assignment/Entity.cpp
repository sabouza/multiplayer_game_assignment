#include <hppFiles/Entity.hpp>

#include <cassert>


Entity::Entity(int hitAmount)
: Velocity()
, _hitAmount(hitAmount)
{
}

void Entity::SetVelocity(sf::Vector2f velocity)
{
	Velocity = velocity;
}

void Entity::SetVelocity(float vx, float vy)
{
	Velocity.x = vx;
	Velocity.y = vy;
}

sf::Vector2f Entity::GetVelocity() const
{
	return Velocity;
}

void Entity::AccelerateVelocity(sf::Vector2f velocity)
{
	Velocity += velocity;
}

void Entity::AccelerateVelocity(float vx, float vy)
{
	Velocity.x += vx;
	Velocity.y += vy;
}

int Entity::GetHitAmount() const
{
	return _hitAmount;
}

void Entity::SetHitAmount(int points)
{
	assert(points > 0);
	_hitAmount = points;
}


void Entity::UpdateCurrent(sf::Time dt, CommandsQueue&)
{	
	move(Velocity * dt.asSeconds());
}
