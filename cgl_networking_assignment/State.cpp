#include <hppFiles/State.hpp>
#include <hppFiles/StateStack.hpp>


State::Context::Context(sf::RenderWindow& window, TextureHolder& textures, FontHolder& fonts, KeyBinding& keys1, KeyBinding& keys2)
: window(&window)
, textures(&textures)
, fonts(&fonts)
, keys1(&keys1)
, keys2(&keys2)
{
}

State::State(StateStack& stack, Context context)
: stateStack(&stack)
, _context(context)
{
}

State::~State()
{
}

void State::RequestPushStack(States::ID stateID)
{
	stateStack->pushState(stateID);
}

void State::RequestPopState()
{
	stateStack->popState();
}

void State::RequestClearState()
{
	stateStack->clearStates();
}

State::Context State::getContext() const
{
	return _context;
}

void State::OnActivate()
{

}

void State::OnDestroy()
{

}
