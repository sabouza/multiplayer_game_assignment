#include <hppFiles/Player.hpp>
#include <hppFiles/CommandsQueue.hpp>
#include <hppFiles/PlayerPawn.hpp>
#include <hppFiles/Foreach.hpp>
#include <hppFiles/NetworkProtocol.hpp>

#include <SFML/Network/Packet.hpp>

#include <map>
#include <string>
#include <algorithm>


using namespace std::placeholders;

struct PlayerMovement
{
	PlayerMovement(float velocityX, float velocityY, int id)
		: velocity(velocityX, velocityY)
		, PlayerPawnID(id)
	{
	}

	void operator() (PlayerPawn& Player, sf::Time) const
	{
		if (Player.getIdentifier() == PlayerPawnID)
			Player.AccelerateVelocity(velocity * Player.getMaxSpeed());
	}

	sf::Vector2f velocity;
	int PlayerPawnID;
};

Player::Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding)
	: keyBinding(binding)
	, currentGameStatus(moving)
	, playerID(identifier)
	, socket(socket)
{
	ActionInit();
	FOREACH(auto& pair, actionBinding)
		pair.second.category = Category::Player;
}

void Player::ManageEvent(const sf::Event& event, CommandsQueue& commands)
{
	// Event
	if (event.type == sf::Event::KeyPressed)
	{
		Action action;
		if (keyBinding && keyBinding->checkAction(event.key.code, action) && !isRealtimeAction(action))
		{
			if (socket)
			{
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Client::PlayerEvent);
				packet << playerID;
				packet << static_cast<sf::Int32>(action);
				socket->send(packet);
			}

			else
				commands.push(actionBinding[action]);
		}
	}

	// Realtime change (network connected)
	// Send realtime change over network
	if ((event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased) && socket)
	{
		Action action;
		if (keyBinding && keyBinding->checkAction(event.key.code, action) && isRealtimeAction(action))
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
			packet << playerID;
			packet << static_cast<sf::Int32>(action);
			packet << (event.type == sf::Event::KeyPressed);
			socket->send(packet);
		}
	}
}

bool Player::isLocal() const
{
	return keyBinding != nullptr;
}

void Player::DisableRealtimeActions()
{
	FOREACH(auto& action, actionProxies)
	{
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
		packet << playerID;
		packet << static_cast<sf::Int32>(action.first);
		packet << false;
		socket->send(packet);
	}
}

// Check if this is a networked game and local player or just a single player game
// If yes, lookup all actions and push corresponding commands to the queue
void Player::ManageRealtimeInput(CommandsQueue& commands)
{
	if ((socket && isLocal()) || !socket)
	{
		std::vector<Action> activeActions = keyBinding->ReceiveRealtimeAction();
		FOREACH(Action action, activeActions)
			commands.push(actionBinding[action]);
	}
}

//Traverse all realtime input proxies. Because this is a networked game, the input isn't handled directly
void Player::ManageRealtimeNetworkInput(CommandsQueue& commands)
{
	if (socket && !isLocal())
	{
		FOREACH(auto pair, actionProxies)
		{
			if (pair.second && isRealtimeAction(pair.first))
				commands.push(actionBinding[pair.first]);
		}
	}
}

void Player::ManageNetworkEvent(Action action, CommandsQueue& commands)
{
	commands.push(actionBinding[action]);
}

void Player::ManageRealtimeNetworkChange(Action action, bool actionEnabled)
{
	actionProxies[action] = actionEnabled;
}

void Player::SetMissionStat(playerGameStatus status)
{
	currentGameStatus = status;
}

Player::playerGameStatus Player::GetMissionStat() const
{
	return currentGameStatus;
}

void Player::ActionInit()
{
	actionBinding[PlayerAction::moveLeft].action = derivedAction<PlayerPawn>(PlayerMovement(-1, 0, playerID));
	actionBinding[PlayerAction::moveRight].action = derivedAction<PlayerPawn>(PlayerMovement(+1, 0, playerID));
	actionBinding[PlayerAction::moveUp].action = derivedAction<PlayerPawn>(PlayerMovement(0, -1, playerID));
	actionBinding[PlayerAction::moveDown].action = derivedAction<PlayerPawn>(PlayerMovement(0, +1, playerID));
}
