#include "Ball.hpp"
#include <hppFiles/DataTables.hpp>
#include <hppFiles/Category.hpp>
#include <hppFiles/CommandsQueue.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/ResourceContainer.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <stdio.h>


Ball::Ball(const TextureHolder& textures, bool isHost)
	: Entity(1)
	, Sprite(textures.get(Textures::Ball))
{
	CenterObjectOrigin(Sprite);
	reset();
	IsHost = isHost;
}

void Ball::reset() {
	setPosition(500.f, 4580.f);
	SetVelocity(0.f, 0.f);
	clock.restart();
}

void Ball::UpdateCurrent(sf::Time dt, CommandsQueue& commands)
{
	//If the ball hits the lines of the playfield, the ball collides back
	float stopSpeed = 0.95f;
	if (getPosition().x <= 90 || getPosition().x >= 930) {
		BounceOffVerticalLines();
	}
	if (getPosition().y <= 4320 || getPosition().y >= 4900) {
		BounceOffHorizontalLines();
	}


	if (clock.getElapsedTime() > sf::milliseconds(100)) {

		if (IsHost) {
			if (GetVelocity().x > 0) {
				SetVelocity(GetVelocity().x *stopSpeed, GetVelocity().y);
			}

			if (GetVelocity().x < 0) {
				SetVelocity(GetVelocity().x *stopSpeed, GetVelocity().y);
			}

			if (GetVelocity().y > 0) {
				SetVelocity(GetVelocity().x, GetVelocity().y*stopSpeed);
			}

			if (GetVelocity().y < 0) {
				SetVelocity(GetVelocity().x, GetVelocity().y*stopSpeed);
			}
		}

		clock.restart();
	}

	Entity::UpdateCurrent(dt, commands);
}

void Ball::BounceOffVerticalLines() {
	SetVelocity(-GetVelocity().x,GetVelocity().y);
}

void Ball::BounceOffHorizontalLines() {
	SetVelocity(GetVelocity().x, -GetVelocity().y);
}

unsigned int Ball::getCategory() const
{
	return Category::Ball;
}

sf::FloatRect Ball::getBoundingRect() const
{
	return getWorldTransform().transformRect(Sprite.getGlobalBounds());
}

void Ball::DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(Sprite, states);
}
