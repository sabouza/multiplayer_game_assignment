#include <hppFiles/GameApp.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/State.hpp>
#include <hppFiles/StateIdentifiers.hpp>
#include <hppFiles/TitleState.hpp>
#include <hppFiles/GameState.hpp>
#include <hppFiles/MultiplayerState.hpp>
#include <hppFiles/MenuState.hpp>
#include <hppFiles/PauseGame.hpp>
#include <hppFiles/GameOverState.hpp>


const sf::Time GameApp::TimePerFrame = sf::seconds(1.f/60.f);

GameApp::GameApp()
: Window(sf::VideoMode(1024, 768), "CGL Assignment - Sarah Abouzari - 2017/18", sf::Style::Close)
, Textures()
, Fonts()
, keyBinding1(1)
, keyBinding2(2)
, stateStack(State::Context(Window, Textures, Fonts, keyBinding1, keyBinding2))
, StatisticText()
, StatisticTime()
, StatisticFPS(0)
{
	Window.setKeyRepeatEnabled(false);
	Window.setVerticalSyncEnabled(true);
	Fonts.LoadResource(Fonts::Bebas, 	"Media/BEBAS__.ttf");

	Textures.LoadResource(Textures::MenuBackground,	"Media/Textures/MenuBackground.png");
	Textures.LoadResource(Textures::Buttons,		"Media/Textures/Buttons.png");

	StatisticText.setFont(Fonts.get(Fonts::Bebas));
	StatisticText.setPosition(5.f, 5.f);
	StatisticText.setCharacterSize(10u);

	RegisterState();
	stateStack.pushState(States::Title);
}

void GameApp::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (Window.isOpen())
	{
		sf::Time dt = clock.restart();
		timeSinceLastUpdate += dt;
		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;

			ProcessInput();
			Update(TimePerFrame);

			if (stateStack.isEmpty())
				Window.close();
		}

		UpdateStatistic(dt);
		Render();
	}
}

void GameApp::ProcessInput()
{
	sf::Event event;
	while (Window.pollEvent(event))
	{
		stateStack.ManageEvent(event);

		if (event.type == sf::Event::Closed)
			Window.close();
	}
}

void GameApp::Update(sf::Time dt)
{
	stateStack.Update(dt);
}

void GameApp::Render()
{
	Window.clear();

	stateStack.draw();

	Window.setView(Window.getDefaultView());
	Window.draw(StatisticText);

	Window.display();
}

void GameApp::UpdateStatistic(sf::Time dt)
{
	StatisticTime += dt;
	StatisticFPS += 1;
	if (StatisticTime >= sf::seconds(1.0f))
	{
		StatisticText.setString("FPS: " + toString(StatisticFPS));

		StatisticTime -= sf::seconds(1.0f);
		StatisticFPS = 0;
	}
}

void GameApp::RegisterState()
{
	stateStack.registerState<TitleState>(States::Title);
	stateStack.registerState<MenuState>(States::Menu);
	stateStack.registerState<GameState>(States::Game);
	stateStack.registerState<MultiplayerState>(States::HostGame, true);
	stateStack.registerState<MultiplayerState>(States::JoinGame, false);
	stateStack.registerState<PauseGame>(States::Pause);
	stateStack.registerState<PauseGame>(States::NetworkPause, true);
	stateStack.registerState<GameOverState>(States::GameOver, "Lost!");
	stateStack.registerState<GameOverState>(States::won, "Won!");
}
