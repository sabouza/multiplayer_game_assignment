#include <algorithm>
#include <cmath>
#include <limits>
#include <hppFiles/GameField.hpp>
#include <hppFiles/Foreach.hpp>
#include <hppFiles/TextNode.hpp>
#include <hppFiles/NetworkNode.hpp>
#include <hppFiles/Utility.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

GameField::GameField(sf::RenderTarget& outputTarget, FontHolder& fonts, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, WorldView(outputTarget.getDefaultView())
	, Textures()
	, Fonts(fonts)
	, worldGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, WorldView.getSize().x, 5000.f)
	, StartPosition(WorldView.getSize().x / 2.f, mWorldBounds.height - WorldView.getSize().y / 2.f)
	, mPlayerPawns()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
{
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();

	// Prepare the view
	WorldView.setCenter(StartPosition);

	Player1ScoreText.setFont(Fonts.get(Fonts::Bebas));
	Player1ScoreText.setPosition(mTarget.getSize().x / 2 / 2, mTarget.getSize().y - 115);

	Player2ScoreText.setFont(Fonts.get(Fonts::Bebas));
	Player2ScoreText.setPosition((mTarget.getSize().x / 2) + 150, mTarget.getSize().y - 115);

}


void GameField::Update(sf::Time dt)
{

	FOREACH(PlayerPawn* a, mPlayerPawns)
		a->SetVelocity(0.f, 0.f);
	// Forward commands to scene graph, adapt velocity (diagonal correction)
	while (!mCommandQueue.isEmpty())
		worldGraph.OnCommand(mCommandQueue.pop(), dt);
	adaptPlayerVelocity();
	// Collision detection and response (may destroy entities)
	DetectCollision();
	// Remove all entities/playerpawns if destroyed and create new ones
	auto firstToRemove = std::remove_if(mPlayerPawns.begin(), mPlayerPawns.end(), std::mem_fn(&PlayerPawn::isMarkedForRemoval));
	mPlayerPawns.erase(firstToRemove, mPlayerPawns.end());
	worldGraph.RemoveWreck();
	worldGraph.Update(dt, mCommandQueue);
	LimitPlayerPosition();

	Player1ScoreText.setString("Player 1:  " + std::to_string(Player1ScoreInt));
	Player2ScoreText.setString("Player 2:  " + std::to_string(Player2ScoreInt));
}

void GameField::draw()
{
		mSceneTexture.clear();
		mSceneTexture.setView(WorldView);
		mSceneTexture.draw(worldGraph);
		mSceneTexture.display();
		processedTextures.ApplyTexture(mSceneTexture, mTarget);
		mTarget.draw(Player1ScoreText);
		mTarget.draw(Player2ScoreText);
		mTarget.draw(mStatusText);
}

CommandsQueue& GameField::getCommandQueue()
{
	return mCommandQueue;
}

PlayerPawn* GameField::getPlayerPawn(int identifier) const
{
	FOREACH(PlayerPawn* a, mPlayerPawns)
	{
		if (a->getIdentifier() == identifier)
			return a;
	}

	return nullptr;
}

void GameField::removePlayerPawn(int identifier)
{
	PlayerPawn* playerpawn = getPlayerPawn(identifier);
	if (playerpawn)
	{
		mPlayerPawns.erase(std::find(mPlayerPawns.begin(), mPlayerPawns.end(), playerpawn));
	}
}

PlayerPawn* GameField::addPlayerPawn(int identifier)
{
	std::unique_ptr<PlayerPawn> player(new PlayerPawn(PlayerPawn::Blue, Textures, Fonts));

	switch (identifier) {
	case 1:
		player->setPosition(450.f, 4620.f);
		break;
	case 2:
		player->setPosition(450.f, 4520.f);
		break;
	case 3:
		player->setPosition(600.f, 4620.f);
		break;
	case 4:
		player->setPosition(600.f, 4520.f);
		break;
	default:
		player->setPosition(450.f, 4620.f);
		break;
	}

	player->SetIdentifier(identifier);
	printf("%i", identifier);
	mPlayerPawns.push_back(player.get());
	mSceneLayers[UpperAir]->AttachChild(std::move(player));
	return mPlayerPawns.back();
}

void GameField::setCurrentBattleFieldPosition(float lineY)
{
	WorldView.setCenter(WorldView.getCenter().x, lineY - WorldView.getSize().y / 2);
	StartPosition.y = mWorldBounds.height;
}

void GameField::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

bool GameField::hasAlivePlayer() const
{
	return mPlayerPawns.size() > 0;
}

bool GameField::hasPlayerReachedEnd() const
{
	if (PlayerPawn* playerpawn = getPlayerPawn(1))
		return !mWorldBounds.contains(playerpawn->getPosition());
	else
		return false;
}

void GameField::loadTextures()
{
	Textures.LoadResource(Textures::Player1, "Media/Textures/Player1.png");
	Textures.LoadResource(Textures::Player2, "Media/Textures/Player2.png");
	Textures.LoadResource(Textures::Ball, "Media/Textures/Ball.png");
	Textures.LoadResource(Textures::Background, "Media/Textures/FootballfieldCollage.png");
}

void GameField::LimitPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 40.f;

	FOREACH(PlayerPawn* playerpawn, mPlayerPawns)
	{
		sf::Vector2f position = playerpawn->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistance);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
		position.y = std::max(position.y, viewBounds.top + borderDistance);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		playerpawn->setPosition(position);
	}
}

void GameField::adaptPlayerVelocity()
{
	FOREACH(PlayerPawn* playerpawn, mPlayerPawns)
	{
		sf::Vector2f velocity = playerpawn->GetVelocity();

		// If moving diagonally, reduce velocity (to have always same velocity)
		if (velocity.x != 0.f && velocity.y != 0.f)
			playerpawn->SetVelocity(velocity / std::sqrt(2.f));

	}
}

bool matchesCategories(WorldNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void GameField::DetectCollision()
{
	std::set<WorldNode::Pair> collisionPairs;
	worldGraph.CheckSceneCollision(worldGraph, collisionPairs);

	if (soccerBallG) {
		if (soccerBallG->getPosition().y > 4495 && soccerBallG->getPosition().y < 4650) {
			if (soccerBallG->getPosition().x >= 875) {
				static_cast<Ball&>(*soccerBallG).reset();
				Player1ScoreInt++;
			}
			if (soccerBallG->getPosition().x <= 122) {
				static_cast<Ball&>(*soccerBallG).reset();
				Player2ScoreInt++;
			}
		}
	}

	FOREACH(WorldNode::Pair pair, collisionPairs)
	{
		if (matchesCategories(pair, Category::Player, Category::Ball))
		{
			auto& player = static_cast<PlayerPawn&>(*pair.first);
			auto& soccerBall = static_cast<Ball&>(*pair.second);

			if (player.getCategory() == 2) {
				const float strength = 0.4f; //adjust this to control amount of power added to ball

				sf::Vector2f direction = soccerBall.getPosition() - player.getPosition();

				if (isHost) {
					// In case you are the host, you can set the velocity of the ball directly.
					// Kicking works by going to the ball, and pushing it away
					soccerBall.SetVelocity(soccerBall.GetVelocity() + (direction * strength));
				}

			}
		}
	}
}

void GameField::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		WorldNode::Ptr layer(new WorldNode(category));
		mSceneLayers[i] = layer.get();

		worldGraph.AttachChild(std::move(layer));
	}

	//spawnCenterBall();

	// Prepare the background
	sf::Texture& backgroundTexture = Textures.get(Textures::Background);

	std::unique_ptr<SpriteNode> backgroundSprite(new SpriteNode(backgroundTexture));
	backgroundSprite->setPosition(.0f, 4230.0f);
	mSceneLayers[Background]->AttachChild(std::move(backgroundSprite));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		worldGraph.AttachChild(std::move(networkNode));
	}


}

void GameField::spawnCenterBall(bool isHost)
{
	std::unique_ptr<Ball> soccerBallT(new Ball(Textures, isHost));

	soccerBallG = soccerBallT.get();

	mSceneLayers[UpperAir]->AttachChild(std::move(soccerBallT));

}


sf::FloatRect GameField::getViewBounds() const
{
	return sf::FloatRect(WorldView.getCenter() - WorldView.getSize() / 2.5f, WorldView.getSize() / 1.25f);
}

sf::FloatRect GameField::getSoccerfieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}
