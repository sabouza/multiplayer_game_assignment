#include <hppFiles/CommandsQueue.hpp>
#include <hppFiles/WorldNode.hpp>


void CommandsQueue::push(const Command& command)
{
	mQueue.push(command);
}

Command CommandsQueue::pop()
{
	Command command = mQueue.front();
	mQueue.pop();
	return command;
}

bool CommandsQueue::isEmpty() const
{
	return mQueue.empty();
}
