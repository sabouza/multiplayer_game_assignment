#include <hppFiles/PauseGame.hpp>
#include <hppFiles/MenuButtons.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


PauseGame::PauseGame(StateStack& stack, Context context, bool letUpdatesThrough)
: State(stack, context)
, backgroundSprite()
, mPausedText()
, guiContainer()
, mLetUpdatesThrough(letUpdatesThrough)
{
	sf::Font& font = context.fonts->get(Fonts::Bebas);
	sf::Vector2f windowSize(context.window->getSize());

	mPausedText.setFont(font);
	mPausedText.setString("Game  Paused");	
	mPausedText.setCharacterSize(70);
	CenterObjectOrigin(mPausedText);
	mPausedText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	auto returnButton = std::make_shared<GUI::MenuButtons>(context);
	returnButton->setPosition(windowSize.x /2 - 105, 0.4f * windowSize.y + 80);
	returnButton->SetText("Return  To  Game");
	returnButton->SetCallback([this] ()
	{
		RequestPopState();
	});

	auto menuButton = std::make_shared<GUI::MenuButtons>(context);
	menuButton->setPosition(windowSize.x / 2 - 105, 0.4f * windowSize.y + 150);
	menuButton->SetText("Quit  To  Menu");
	menuButton->SetCallback([this] ()
	{
		RequestClearState();
		RequestPushStack(States::Menu);
	});

	guiContainer.pack(returnButton);
	guiContainer.pack(menuButton);
}

PauseGame::~PauseGame()
{
}

void PauseGame::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(mPausedText);
	window.draw(guiContainer);
}

bool PauseGame::Update(sf::Time)
{
	return mLetUpdatesThrough;
}

bool PauseGame::ManageEvent(const sf::Event& event)
{
	guiContainer.ManageEvent(event);
	return false;
}
