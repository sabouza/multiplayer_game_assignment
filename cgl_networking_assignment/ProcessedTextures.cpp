#include <hppFiles/ProcessedTextures.hpp>
#include <SFML/Graphics/VertexArray.hpp>


ProcessedTextures::ProcessedTextures()
	: shaders()
	, RenderTexture()
	, primaryTextures()
	, secondaryTextures()
{
	shaders.LoadResource(Shaders::AddPass, "Media/Shaders/Fullpass.vert", "Media/Shaders/Add.frag");
}

void ProcessedTextures::ApplyTexture(const sf::RenderTexture& input, sf::RenderTarget& output)
{
	PrepareTexture(input.getSize());
	AddToTexture(primaryTextures[0], secondaryTextures[0], primaryTextures[1]);
	primaryTextures[1].display();
	AddToTexture(input, primaryTextures[1], output);
}

void ProcessedTextures::PrepareTexture(sf::Vector2u size)
{
	if (RenderTexture.getSize() != size)
	{
		RenderTexture.create(size.x, size.y);
		primaryTextures[0].create(size.x, size.y);
		primaryTextures[1].create(size.x, size.y);
		secondaryTextures[0].create(size.x, size.y);
		secondaryTextures[1].create(size.x, size.y);
	}
}

void ProcessedTextures::AddToTexture(const sf::RenderTexture& source, const sf::RenderTexture& bloom, sf::RenderTarget& output)
{
	sf::Shader& adder = shaders.get(Shaders::AddPass);
	adder.setParameter("source", source.getTexture());
	DrawTextures(adder, output);
}

void ProcessedTextures::DrawTextures(const sf::Shader& shader, sf::RenderTarget& output)
{
	sf::Vector2f outputSize = static_cast<sf::Vector2f>(output.getSize());

	sf::VertexArray vertices(sf::TrianglesStrip, 4);
	vertices[0] = sf::Vertex(sf::Vector2f(0, 0), sf::Vector2f(0, 1));
	vertices[1] = sf::Vertex(sf::Vector2f(outputSize.x, 0), sf::Vector2f(1, 1));
	vertices[2] = sf::Vertex(sf::Vector2f(0, outputSize.y), sf::Vector2f(0, 0));
	vertices[3] = sf::Vertex(sf::Vector2f(outputSize), sf::Vector2f(1, 0));

	sf::RenderStates states;
	states.shader = &shader;

	output.draw(vertices, states);
}
