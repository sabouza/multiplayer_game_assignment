#include <cmath>
#include <hppFiles/PlayerPawn.hpp>
#include <hppFiles/DataTables.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/CommandsQueue.hpp>
#include <hppFiles/NetworkNode.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

using namespace std::placeholders;

namespace
{
	const std::vector<PlayerPawnData> Table = InitPlayerPawnData();
}

PlayerPawn::PlayerPawn(Type type, const TextureHolder& textures, const FontHolder& fonts)
	: Entity(Table[type].hitAmount)
	, type(type)
	, Sprite(textures.get(Table[type].texture), Table[type].textureRect)
	, playerID(0)
{
	CenterObjectOrigin(Sprite);
}

void PlayerPawn::DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(Sprite, states);
}


void PlayerPawn::UpdateCurrent(sf::Time dt, CommandsQueue& commands)
{
	// Update texts and roll animation
	UpdateTexts();
	UpdateAnimation();
	Entity::UpdateCurrent(dt, commands);
}

unsigned int PlayerPawn::getCategory() const
{
	if (isAllied())
		return Category::Player;
	else
		return Category::EnemyPawn;
}

sf::FloatRect PlayerPawn::getBoundingRect() const
{
	return getWorldTransform().transformRect(Sprite.getGlobalBounds());
}

bool PlayerPawn::isMarkedForRemoval() const
{
	return false;
}

bool PlayerPawn::isAllied() const
{
	return type == Blue;
}

float PlayerPawn::getMaxSpeed() const
{
	return Table[type].speed;
}

int	PlayerPawn::getIdentifier()
{
	return playerID;
}

void PlayerPawn::SetIdentifier(int identifier)
{
	playerID = identifier;
}


void PlayerPawn::UpdateTexts()
{

}

void PlayerPawn::UpdateAnimation()
{
	if (Table[type].hasAnimation)
	{
		sf::IntRect textureRect = Table[type].textureRect;

		// Roll left: Texture rect offset once
		if (GetVelocity().x < 0.f)
			textureRect.left += textureRect.width;

		// Roll right: Texture rect offset twice
		else if (GetVelocity().x > 0.f)
			textureRect.left += 2 * textureRect.width;

		Sprite.setTextureRect(textureRect);
	}
}
