#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <hppFiles/GameContainer.hpp>
#include <hppFiles/Foreach.hpp>


namespace GUI
{

	Container::Container()
		: childrenContainer()
		, selectedChild(-1)
	{
	}

	void Container::pack(Component::Ptr component)
	{
		childrenContainer.push_back(component);

		if (!hasSelection() && component->isSelectable())
			Select(childrenContainer.size() - 1);
	}

	bool Container::isSelectable() const
	{
		return false;
	}

	// SELECTION BEHAVIOUR OF BUTTONS AND MENU THROUGH KEYBOARD INPUT
	void Container::ManageEvent(const sf::Event& event)
	{
		if (hasSelection() && childrenContainer[selectedChild]->isActive())
			childrenContainer[selectedChild]->ManageEvent(event);
		else if (event.type == sf::Event::KeyReleased)
		{
			if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
				SelectPreviousComponent();
			else if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
				SelectNextComponent();
			else if (event.key.code == sf::Keyboard::Return || event.key.code == sf::Keyboard::Space)
				if (hasSelection())
					childrenContainer[selectedChild]->Activate();
		}
	}

	void Container::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();

		FOREACH(const Component::Ptr& child, childrenContainer)
			target.draw(*child, states);
	}

	bool Container::hasSelection() const
	{
		return selectedChild >= 0;
	}

	void Container::Select(std::size_t index)
	{
		if (childrenContainer[index]->isSelectable())
		{
			if (hasSelection())
				childrenContainer[selectedChild]->Deselect();

			childrenContainer[index]->Select();
			selectedChild = index;
		}
	}

	//Search for the next selectable component and wrap around if necessary
	void Container::SelectNextComponent()
	{
		if (!hasSelection())
			return;
		int next = selectedChild;
		do
			next = (next + 1) % childrenContainer.size();
		while (!childrenContainer[next]->isSelectable());
		Select(next);
	}

	void Container::SelectPreviousComponent()
	{
		if (!hasSelection())
			return;

		// Search previous component that is selectable, wrap around if necessary
		int prev = selectedChild;
		do
			prev = (prev + childrenContainer.size() - 1) % childrenContainer.size();
		while (!childrenContainer[prev]->isSelectable());

		// Select that component
		Select(prev);
	}

}
