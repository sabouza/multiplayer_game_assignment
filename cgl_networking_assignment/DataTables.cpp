#include <hppFiles/DataTables.hpp>
#include <hppFiles/PlayerPawn.hpp>


// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;

std::vector<PlayerPawnData> InitPlayerPawnData()
{
	std::vector<PlayerPawnData> data(PlayerPawn::TypeCount);

	data[PlayerPawn::Blue].speed = 200.f;
	data[PlayerPawn::Blue].texture = Textures::Player1;
	data[PlayerPawn::Blue].textureRect = sf::IntRect(0, 0, 40, 120);
	data[PlayerPawn::Blue].hasAnimation = true;

	data[PlayerPawn::Red].speed = 200.f;
	data[PlayerPawn::Red].texture = Textures::Player2;
	data[PlayerPawn::Red].textureRect = sf::IntRect(0, 0, 40, 120);
	data[PlayerPawn::Red].hasAnimation = true;

	return data;
}



