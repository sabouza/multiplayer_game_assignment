#include <hppFiles/TitleState.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/ResourceContainer.hpp>

#include <SFML/Graphics/RenderWindow.hpp>


TitleState::TitleState(StateStack& stack, Context context)
: State(stack, context)
, Text()
, showText(true)
, textBlinkTime(sf::Time::Zero)
{
	backgroundSprite.setTexture(context.textures->get(Textures::MenuBackground));

	Text.setFont(context.fonts->get(Fonts::Bebas));
	Text.setString("Press  Any  Key");
	CenterObjectOrigin(Text);
	Text.setPosition(sf::Vector2f(context.window->getSize() / 2u));
}

void TitleState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.draw(backgroundSprite);

	if (showText)
		window.draw(Text);
}

bool TitleState::Update(sf::Time dt)
{
	textBlinkTime += dt;

	if (textBlinkTime >= sf::seconds(0.5f))
	{
		showText = !showText;
		textBlinkTime = sf::Time::Zero;
	}

	return true;
}

bool TitleState::ManageEvent(const sf::Event& event)
{
	// If any key is pressed, trigger the next screen
	if (event.type == sf::Event::KeyReleased)
	{
		RequestPopState();
		RequestPushStack(States::Menu);
	}

	return true;
}