#include <hppFiles/Label.hpp>
#include <hppFiles/Utility.hpp>

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>


namespace GUI
{

Label::Label(const std::string& text, const FontHolder& fonts)
: Text(text, fonts.get(Fonts::Bebas), 16)
{
}

bool Label::isSelectable() const
{
    return false;
}

void Label::handleEvent(const sf::Event&)
{
}

void Label::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(Text, states);
}

void Label::SetText(const std::string& text)
{
	Text.setString(text);
}

}