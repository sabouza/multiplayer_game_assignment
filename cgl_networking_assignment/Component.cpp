#include <hppFiles/Component.hpp>

namespace GUI
{

Component::Component()
: _isSelected(false)
, _isActive(false)
{
}

Component::~Component()
{
}

bool Component::isSelected() const
{
	return _isSelected;
}

void Component::Select()
{
	_isSelected = true;
}

void Component::Deselect()
{
	_isSelected = false;
}

bool Component::isActive() const
{
	return _isActive;
}

void Component::Activate()
{
	_isActive = true;
}

void Component::Deactivate()
{
	_isActive = false;
}

}