#include <hppFiles/MenuState.hpp>
#include <hppFiles/MenuButtons.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/ResourceContainer.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


MenuState::MenuState(StateStack& stack, Context context)
: State(stack, context)
, guiContainer()
{
	sf::Texture& texture = context.textures->get(Textures::MenuBackground);
	backgroundSprite.setTexture(texture);

	auto hostPlayButton = std::make_shared<GUI::MenuButtons>(context);
	hostPlayButton->SetText("Host  Game");
	hostPlayButton->setPosition(407, 310);
	hostPlayButton->SetCallback([this] ()
	{
		RequestPopState();
		RequestPushStack(States::HostGame);
	});

	auto joinPlayButton = std::make_shared<GUI::MenuButtons>(context);
	joinPlayButton->SetText("Join  Game");
	joinPlayButton->setPosition(407, 380);
	joinPlayButton->SetCallback([this] ()
	{
		RequestPopState();
		RequestPushStack(States::JoinGame);
	});

	auto exitButton = std::make_shared<GUI::MenuButtons>(context);
	exitButton->setPosition(407, 480);
	exitButton->SetText("Exit");
	exitButton->SetCallback([this] ()
	{
		RequestPopState();
	});

	//guiContainer.pack(playButton);
	guiContainer.pack(hostPlayButton);
	guiContainer.pack(joinPlayButton);
	guiContainer.pack(exitButton);
}

void MenuState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.setView(window.getDefaultView());

	window.draw(backgroundSprite);
	window.draw(guiContainer);
}

bool MenuState::Update(sf::Time)
{
	return true;
}

bool MenuState::ManageEvent(const sf::Event& event)
{
	guiContainer.ManageEvent(event);
	return false;
}
