#include <hppFiles/GameState.hpp>
#include <SFML/Graphics/RenderWindow.hpp>


GameState::GameState(StateStack& stack, Context context)
: State(stack, context)
, world(*context.window, *context.fonts, false)
, _Player(nullptr, 1, context.keys1)
{
	world.addPlayerPawn(1);
	world.spawnCenterBall(true);
	world.isHost = true;
	_Player.SetMissionStat(Player::moving);
}

void GameState::draw()
{
	world.draw();
}

bool GameState::Update(sf::Time dt)
{
	world.Update(dt);

	if (!world.hasAlivePlayer())
	{
		_Player.SetMissionStat(Player::losing);
		RequestPushStack(States::GameOver);
	}

	CommandsQueue& commands = world.getCommandQueue();
	_Player.ManageRealtimeInput(commands);

	return true;
}

bool GameState::ManageEvent(const sf::Event& event)
{
	// Game input handling
	CommandsQueue& commands = world.getCommandQueue();
	_Player.ManageEvent(event, commands);

	// Escape pressed, trigger the pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		RequestPushStack(States::Pause);

	return true;
}