#include <hppFiles/StateStack.hpp>
#include <hppFiles/Foreach.hpp>

#include <cassert>


StateStack::StateStack(State::Context context)
: stateStack()
, pendingList()
, _context(context)
, factories()
{
}

void StateStack::Update(sf::Time dt)
{
	// Iterate from top to bottom, stop as soon as Update() returns false
	for (auto itr = stateStack.rbegin(); itr != stateStack.rend(); ++itr)
	{
		if (!(*itr)->Update(dt))
			break;
	}

	ApplyIncomingChanges();
}

void StateStack::draw()
{
	// draw all active states from bottom to top
	FOREACH(State::Ptr& state, stateStack)
		state->draw();
}

void StateStack::ManageEvent(const sf::Event& event)
{
	// Iterate from top to bottom, stop as soon as ManageEvent() returns false
	for (auto itr = stateStack.rbegin(); itr != stateStack.rend(); ++itr)
	{
		if (!(*itr)->ManageEvent(event))
			break;
	}

	ApplyIncomingChanges();
}

void StateStack::pushState(States::ID stateID)
{
	pendingList.push_back(IncomingChange(Push, stateID));
}

void StateStack::popState()
{
	pendingList.push_back(IncomingChange(Pop));
}

void StateStack::clearStates()
{
	pendingList.push_back(IncomingChange(Clear));
}

bool StateStack::isEmpty() const
{
	return stateStack.empty();
}

State::Ptr StateStack::createState(States::ID stateID)
{
	auto found = factories.find(stateID);
	assert(found != factories.end());

	return found->second();
}

void StateStack::ApplyIncomingChanges()
{
	FOREACH(IncomingChange change, pendingList)
	{
		switch (change.action)
		{
			case Push:
				stateStack.push_back(createState(change.stateID));
				break;

			case Pop:
				stateStack.back()->OnDestroy();
				stateStack.pop_back();

				if (!stateStack.empty())
					stateStack.back()->OnActivate();
				break;

			case Clear:
				FOREACH(State::Ptr& state, stateStack)
					state->OnDestroy();

				stateStack.clear();
				break;
		}
	}

	pendingList.clear();
}

StateStack::IncomingChange::IncomingChange(Action action, States::ID stateID)
: action(action)
, stateID(stateID)
{
}
