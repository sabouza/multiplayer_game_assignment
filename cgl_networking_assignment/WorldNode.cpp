#include <algorithm>
#include <cassert>
#include <cmath>
#include <hppFiles/WorldNode.hpp>
#include <hppFiles/Commands.hpp>
#include <hppFiles/Foreach.hpp>
#include <hppFiles/Utility.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>



WorldNode::WorldNode(Category::Type category)
	: childrenContainer()
	, mParent(nullptr)
	, mDefaultCategory(category)
{
}

void WorldNode::AttachChild(Ptr child)
{
	child->mParent = this;
	childrenContainer.push_back(std::move(child));
}

WorldNode::Ptr WorldNode::DetachChild(const WorldNode& node)
{
	auto found = std::find_if(childrenContainer.begin(), childrenContainer.end(), [&](Ptr& p) { return p.get() == &node; });
	assert(found != childrenContainer.end());

	Ptr result = std::move(*found);
	result->mParent = nullptr;
	childrenContainer.erase(found);
	return result;
}

void WorldNode::Update(sf::Time dt, CommandsQueue& commands)
{
	UpdateCurrent(dt, commands);
	UpdateChildren(dt, commands);
}

void WorldNode::UpdateCurrent(sf::Time, CommandsQueue&)
{
	// Do nothing by default
}

void WorldNode::UpdateChildren(sf::Time dt, CommandsQueue& commands)
{
	FOREACH(Ptr& child, childrenContainer)
		child->Update(dt, commands);
}

// Apply transform of current node
// Draw node and childrent
void WorldNode::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	DrawCurrent(target, states);
	DrawChildren(target, states);
}

void WorldNode::DrawCurrent(sf::RenderTarget&, sf::RenderStates) const
{
	//Do Nothing in normal state
}

void WorldNode::DrawChildren(sf::RenderTarget& target, sf::RenderStates states) const
{
	FOREACH(const Ptr& child, childrenContainer)
		child->draw(target, states);
}

void WorldNode::DrawBoundingRect(sf::RenderTarget& target, sf::RenderStates) const
{
	sf::FloatRect rect = getBoundingRect();

	sf::RectangleShape shape;
	shape.setPosition(sf::Vector2f(rect.left, rect.top));
	shape.setSize(sf::Vector2f(rect.width, rect.height));
	shape.setFillColor(sf::Color::Transparent);
	shape.setOutlineColor(sf::Color::Green);
	shape.setOutlineThickness(1.f);

	target.draw(shape);
}

sf::Vector2f WorldNode::getWorldPosition() const
{
	return getWorldTransform() * sf::Vector2f();
}

sf::Transform WorldNode::getWorldTransform() const
{
	sf::Transform transform = sf::Transform::Identity;

	for (const WorldNode* node = this; node != nullptr; node = node->mParent)
		transform = node->getTransform() * transform;

	return transform;
}

// Command current node and children if the category matches
void WorldNode::OnCommand(const Command& command, sf::Time dt)
{
	if (command.category & getCategory())
		command.action(*this, dt);
	FOREACH(Ptr& child, childrenContainer)
		child->OnCommand(command, dt);
}

unsigned int WorldNode::getCategory() const
{
	return mDefaultCategory;
}

void WorldNode::CheckSceneCollision(WorldNode& sceneGraph, std::set<Pair>& collisionPairs)
{
	CheckNodeCollision(sceneGraph, collisionPairs);

	FOREACH(Ptr& child, sceneGraph.childrenContainer)
		CheckSceneCollision(*child, collisionPairs);
}

void WorldNode::CheckNodeCollision(WorldNode& node, std::set<Pair>& collisionPairs)
{
	if (this != &node && collision(*this, node) && !isDestroyed() && !node.isDestroyed())
		collisionPairs.insert(std::minmax(this, &node));

	FOREACH(Ptr& child, childrenContainer)
		child->CheckNodeCollision(node, collisionPairs);
}

// Remove all children which request so
void WorldNode::RemoveWreck()
{
	auto wreckfieldBegin = std::remove_if(childrenContainer.begin(), childrenContainer.end(), std::mem_fn(&WorldNode::isMarkedForRemoval));
	childrenContainer.erase(wreckfieldBegin, childrenContainer.end());
	std::for_each(childrenContainer.begin(), childrenContainer.end(), std::mem_fn(&WorldNode::RemoveWreck));
}

sf::FloatRect WorldNode::getBoundingRect() const
{
	return sf::FloatRect();
}

// By default state, remove node if entity is destroyed
bool WorldNode::isMarkedForRemoval() const
{
	return isDestroyed();
}

// By default state, world node does not need to be removed
bool WorldNode::isDestroyed() const
{
	return false;
}

bool collision(const WorldNode& lhs, const WorldNode& rhs)
{
	return lhs.getBoundingRect().intersects(rhs.getBoundingRect());
}

float distance(const WorldNode& lhs, const WorldNode& rhs)
{
	return length(lhs.getWorldPosition() - rhs.getWorldPosition());
}
