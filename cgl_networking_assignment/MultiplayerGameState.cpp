#include <hppFiles/MultiplayerState.hpp>
#include <hppFiles/Foreach.hpp>
#include <hppFiles/Utility.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Network/IpAddress.hpp>

#include <fstream>

// Try to open existing file (RAII block)
//On fail: create new file
sf::IpAddress getAddressFromFile()
{
	{
		std::ifstream inputFile("ip.txt");
		std::string ipAddress;
		if (inputFile >> ipAddress)
			return ipAddress;
	}
	std::ofstream outputFile("ip.txt");
	std::string localAddress = "127.0.0.1";
	outputFile << localAddress;
	return localAddress;
}

MultiplayerState::MultiplayerState(StateStack& stack, Context context, bool isHost)
	: State(stack, context)
	, world(*context.window, *context.fonts, true)
	, Window(*context.window)
	, isConnected(false)
	, server(nullptr)
	, activateState(true)
	, hasFocus(true)
	, isHost(isHost)
	, gameStarted(false)
	, clientTimeout(sf::seconds(2.f))
	, timeSinceLastPacket(sf::seconds(0.f))
{
	broadcastText.setFont(context.fonts->get(Fonts::Bebas));
	broadcastText.setPosition(1024.f / 2, 100.f);


	spawnPlayerText.setString("Press  Enter  for a second Teamplayer");
	spawnPlayerText.setFont(context.fonts->get(Fonts::Bebas));
	spawnPlayerText.setCharacterSize(25);
	spawnPlayerText.setColor(sf::Color::White);
	spawnPlayerText.setPosition(Window.getSize().x / 2.f - (spawnPlayerText.getLocalBounds().width / 2.f), Window.getSize().y - 200);

	//---- SETTING UP CONNECTION TEXT
	connectionText.setString("Trying  to  connect...");
	connectionText.setFont(context.fonts->get(Fonts::Bebas));
	connectionText.setCharacterSize(30);
	connectionText.setColor(sf::Color::White);
	connectionText.setPosition(Window.getSize().x / 2.f - (connectionText.getLocalBounds().width / 2.f), Window.getSize().y / 2.f);
	Window.clear(sf::Color::Black);
	Window.draw(connectionText);
	Window.display();
	connectionText.setString("Connection  Failed!");

	sf::IpAddress ip;
	if (isHost)
	{
		world.isHost = true;
		server.reset(new GameServer(sf::Vector2f(Window.getSize())));
		ip = "127.0.0.1";
	}
	else
	{
		world.isHost = false;
		ip = getAddressFromFile();
	}

	if (socket.connect(ip, ServerPort, sf::seconds(5.f)) == sf::TcpSocket::Done)
		isConnected = true;
	else
		failedConnectiontime.restart();

	socket.setBlocking(false);
}


void MultiplayerState::draw()
{
	if (isConnected)
	{
		world.draw();
		Window.setView(Window.getDefaultView()); //Broadcast messages in default view

		if (!broadcasts.empty())
			Window.draw(broadcastText);

		//Display spawnPlayerText 0.7f seconds (blinking text efffect) until second player has been spawned
		if (localPlayerID.size() < 2 && invitationTime < sf::seconds(0.7f))
			Window.draw(spawnPlayerText);
	}
	else
		Window.draw(connectionText);
}

void MultiplayerState::OnActivate()
{
	activateState = true;
}


bool MultiplayerState::Update(sf::Time dt)
{
	if (isConnected)
	{
		world.Update(dt);

		// Remove players that were destroyed
		bool foundLocalPlayer = false;
		for (auto itr = players.begin(); itr != players.end(); )
		{
			// Check if there are no more local players for remote clients
			if (std::find(localPlayerID.begin(), localPlayerID.end(), itr->first) != localPlayerID.end())
				foundLocalPlayer = true;

			if (!world.getPlayerPawn(itr->first))
			{
				itr = players.erase(itr);

				//If no more players are left
				if (players.empty())
					RequestPushStack(States::GameOver);
			}
			else
				++itr;
		}
		if (!foundLocalPlayer && gameStarted)
			RequestPushStack(States::GameOver);

		// Player Input only works as long as the game is focused and not inside the pausemenu
		if (activateState && hasFocus)
		{
			CommandsQueue& commands = world.getCommandQueue();
			FOREACH(auto& pair, players)
				pair.second->ManageRealtimeInput(commands);
		}

		//Manage Network Input
		CommandsQueue& commands = world.getCommandQueue();
		FOREACH(auto& pair, players)
			pair.second->ManageRealtimeNetworkInput(commands);

		//Manage Messages from Server
		sf::Packet packet;
		if (socket.receive(packet) == sf::Socket::Done)
		{
			timeSinceLastPacket = sf::seconds(0.f);
			sf::Int32 packetType;
			packet >> packetType;
			handlePacket(packetType, packet);
		}
		else
		{
			// Check for timeout with the server
			if (timeSinceLastPacket > clientTimeout)
			{
				isConnected = false;
				connectionText.setString("Lost Connection...");
				CenterObjectOrigin(connectionText);
				failedConnectiontime.restart();
			}
		}

		updateBroadcastMessage(dt);
		//Time counter for blinking text
		invitationTime += dt;
		if (invitationTime > sf::seconds(1.f))
			invitationTime = sf::Time::Zero;
		if (true)
		{
			sf::Packet positionUpdatePacket;
			positionUpdatePacket << static_cast<sf::Int32>(Client::PositionUpdate);
			positionUpdatePacket << static_cast<sf::Int32>(localPlayerID.size());

			FOREACH(sf::Int32 identifier, localPlayerID)
			{
				if (PlayerPawn* soccerpawn = world.getPlayerPawn(identifier))
					positionUpdatePacket << identifier << soccerpawn->getPosition().x << soccerpawn->getPosition().y << world.soccerBallG->getPosition().x << world.soccerBallG->getPosition().y;
			}

			socket.send(positionUpdatePacket);
		}

		timeSinceLastPacket += dt;
	}

	//Return back to menu when player waited for more than 5 seconds to connect..
	else if (failedConnectiontime.getElapsedTime() >= sf::seconds(5.f))
	{
		RequestClearState();
		RequestPushStack(States::Menu);
	}

	return true;
}

void MultiplayerState::DisableRealtimeActions()
{
	activateState = false;

	FOREACH(sf::Int32 identifier, localPlayerID)
		players[identifier]->DisableRealtimeActions();
}

bool MultiplayerState::ManageEvent(const sf::Event& event)
{
	//Game Input Managing
	CommandsQueue& commands = world.getCommandQueue();
	FOREACH(auto& pair, players)
		pair.second->ManageEvent(event, commands);

	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Return && localPlayerID.size() == 1)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::RequestCoopPartner);
			socket.send(packet);
		}

		//Trigger Pause screen on ESCAPE
		else if (event.key.code == sf::Keyboard::Escape)
		{
			DisableRealtimeActions();
			RequestPushStack(States::NetworkPause);
		}
	}
	else if (event.type == sf::Event::GainedFocus)
		hasFocus = true;
	else if (event.type == sf::Event::LostFocus)
		hasFocus = false;
	return true;
}

void MultiplayerState::updateBroadcastMessage(sf::Time elapsedTime)
{
	if (broadcasts.empty())
		return;
	broadcastedTime += elapsedTime;
	// If message has expired, remove it and continue with the next message
	if (broadcastedTime > sf::seconds(2.5f))
	{
		broadcasts.erase(broadcasts.begin());
		if (!broadcasts.empty())
		{
			broadcastText.setString(broadcasts.front());
			CenterObjectOrigin(broadcastText);
			broadcastedTime = sf::Time::Zero;
		}
	}
}

void MultiplayerState::handlePacket(sf::Int32 packetType, sf::Packet& packet)
{
	switch (packetType)
	{
	case Server::BroadcastMessage: //First send message to all connected clients
	{
		std::string message;
		packet >> message;
		broadcasts.push_back(message);

		//Display added message
		if (broadcasts.size() == 1)
		{
			broadcastText.setString(broadcasts.front());
			CenterObjectOrigin(broadcastText);
			broadcastedTime = sf::Time::Zero;
		}
	} break;

	//Spawn player 1 on connect
	case Server::SpawnSelf:
	{
		sf::Int32 soccerpawnIdentifier;
		sf::Vector2f soccerpawnPosition;
		packet >> soccerpawnIdentifier >> soccerpawnPosition.x >> soccerpawnPosition.y;
		PlayerPawn* soccerpawn = world.addPlayerPawn(soccerpawnIdentifier);
		players[soccerpawnIdentifier].reset(new Player(&socket, soccerpawnIdentifier, getContext().keys1));
		localPlayerID.push_back(soccerpawnIdentifier);

		world.spawnCenterBall(isHost);

		gameStarted = true;
	} break;

	case Server::PlayerConnect:
	{
		sf::Int32 soccerpawnIdentifier;
		sf::Vector2f soccerpawnPosition;
		packet >> soccerpawnIdentifier >> soccerpawnPosition.x >> soccerpawnPosition.y;

		PlayerPawn* soccerpawn = world.addPlayerPawn(soccerpawnIdentifier);
		players[soccerpawnIdentifier].reset(new Player(&socket, soccerpawnIdentifier, nullptr));
	} break;

	case Server::PlayerDisconnect:
	{
		sf::Int32 soccerpawnIdentifier;
		packet >> soccerpawnIdentifier;
		players.erase(soccerpawnIdentifier);
	} break;

	case Server::InitialState:
	{
		sf::Int32 soccerpawnCount;
		float worldHeight, currentScroll;
		packet >> worldHeight >> currentScroll;

		world.setWorldHeight(worldHeight);
		world.setCurrentBattleFieldPosition(currentScroll);

		packet >> soccerpawnCount;
		for (sf::Int32 i = 0; i < soccerpawnCount; ++i)
		{
			sf::Int32 soccerpawnIdentifier;
			sf::Int32 hitAmount;
			sf::Vector2f soccerpawnPosition;
			packet >> soccerpawnIdentifier >> soccerpawnPosition.x >> soccerpawnPosition.y >> hitAmount;

			PlayerPawn* soccerpawn = world.addPlayerPawn(soccerpawnIdentifier);
			printf("%f", soccerpawnPosition.x);
			soccerpawn->SetHitAmount(hitAmount);


			players[soccerpawnIdentifier].reset(new Player(&socket, soccerpawnIdentifier, nullptr));
		}
	} break;

	case Server::AcceptCoopPartner:
	{
		sf::Int32 soccerpawnIdentifier;
		packet >> soccerpawnIdentifier;

		world.addPlayerPawn(soccerpawnIdentifier);
		players[soccerpawnIdentifier].reset(new Player(&socket, soccerpawnIdentifier, getContext().keys2));
		localPlayerID.push_back(soccerpawnIdentifier);
	} break;


	case Server::PlayerEvent:
	{
		sf::Int32 soccerpawnIdentifier;
		sf::Int32 action;
		packet >> soccerpawnIdentifier >> action;

		auto itr = players.find(soccerpawnIdentifier);
		if (itr != players.end())
			itr->second->ManageNetworkEvent(static_cast<Player::Action>(action), world.getCommandQueue());
	} break;

	//Player's Movement/Input state
	case Server::PlayerRealtimeChange:
	{
		sf::Int32 soccerpawnIdentifier;
		sf::Int32 action;
		bool actionEnabled;
		packet >> soccerpawnIdentifier >> action >> actionEnabled;

		auto itr = players.find(soccerpawnIdentifier);
		if (itr != players.end())
			itr->second->ManageRealtimeNetworkChange(static_cast<Player::Action>(action), actionEnabled);
	} break;

	case Server::won:
	{
		RequestPushStack(States::won);
	} break;

	case Server::UpdateClientState:
	{
		sf::Vector2f soccerBallPosition;
		sf::Vector2f soccerBallVelocity;

		sf::Int32 soccerpawnCount;
		packet >> soccerBallPosition.x >> soccerBallPosition.y >> soccerBallVelocity.x >> soccerBallVelocity.y >> soccerpawnCount;

		float currentViewPosition = world.getViewBounds().top + world.getViewBounds().height;

		if (!isHost) {
			sf::Vector2f interpolatedPosition2 = world.soccerBallG->getPosition() + (soccerBallPosition - world.soccerBallG->getPosition()) * 0.4f;
			world.soccerBallG->setPosition(interpolatedPosition2);

			sf::Vector2f interpolatedVelocity = world.soccerBallG->GetVelocity() + (soccerBallVelocity - world.soccerBallG->GetVelocity()) * 0.1f;
			world.soccerBallG->SetVelocity(soccerBallVelocity);
		}

		for (sf::Int32 i = 0; i < soccerpawnCount; ++i)
		{
			sf::Vector2f soccerpawnPosition;
			sf::Int32 soccerpawnIdentifier;
			packet >> soccerpawnIdentifier >> soccerpawnPosition.x >> soccerpawnPosition.y;

			PlayerPawn* soccerpawn = world.getPlayerPawn(soccerpawnIdentifier);

			bool isLocalPlayer = std::find(localPlayerID.begin(), localPlayerID.end(), soccerpawnIdentifier) != localPlayerID.end();

			if (soccerpawn && !isLocalPlayer)
			{
				sf::Vector2f interpolatedPosition = soccerpawn->getPosition() + (soccerpawnPosition - soccerpawn->getPosition()) * 0.1f;
				soccerpawn->setPosition(interpolatedPosition);
			}
		}


	} break;
	}
}