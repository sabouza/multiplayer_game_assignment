#include <hppFiles/TextNode.hpp>
#include <hppFiles/Utility.hpp>

#include <SFML/Graphics/RenderTarget.hpp>

    
TextNode::TextNode(const FontHolder& fonts, const std::string& text)
{
	Text.setFont(fonts.get(Fonts::Bebas));
	Text.setCharacterSize(20);
	setString(text);
}

void TextNode::DrawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(Text, states);
}

void TextNode::setString(const std::string& text)
{
	Text.setString(text);
	CenterObjectOrigin(Text);
}