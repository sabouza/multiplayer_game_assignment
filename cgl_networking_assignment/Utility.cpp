//KEYBINDING STRATEGY TO COMPUTE KEY-STRING-NAMES INTO KEYCODES

#include <hppFiles/Utility.hpp>
#include <hppFiles/Animation.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <random>
#include <cmath>
#include <ctime>
#include <cassert>


namespace
{
	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();
}

std::string toString(sf::Keyboard::Key key)
{
	#define GameKeyToString(KEY) case sf::Keyboard::KEY: return #KEY;

	switch (key)
	{
		GameKeyToString(Unknown)
		GameKeyToString(A)
		GameKeyToString(B)
		GameKeyToString(C)
		GameKeyToString(D)
		GameKeyToString(E)
		GameKeyToString(F)
		GameKeyToString(G)
		GameKeyToString(H)
		GameKeyToString(I)
		GameKeyToString(J)
		GameKeyToString(K)
		GameKeyToString(L)
		GameKeyToString(M)
		GameKeyToString(N)
		GameKeyToString(O)
		GameKeyToString(P)
		GameKeyToString(Q)
		GameKeyToString(R)
		GameKeyToString(S)
		GameKeyToString(T)
		GameKeyToString(U)
		GameKeyToString(V)
		GameKeyToString(W)
		GameKeyToString(X)
		GameKeyToString(Y)
		GameKeyToString(Z)
		GameKeyToString(Num0)
		GameKeyToString(Num1)
		GameKeyToString(Num2)
		GameKeyToString(Num3)
		GameKeyToString(Num4)
		GameKeyToString(Num5)
		GameKeyToString(Num6)
		GameKeyToString(Num7)
		GameKeyToString(Num8)
		GameKeyToString(Num9)
		GameKeyToString(Escape)
		GameKeyToString(LControl)
		GameKeyToString(LShift)
		GameKeyToString(LAlt)
		GameKeyToString(LSystem)
		GameKeyToString(RControl)
		GameKeyToString(RShift)
		GameKeyToString(RAlt)
		GameKeyToString(RSystem)
		GameKeyToString(Menu)
		GameKeyToString(LBracket)
		GameKeyToString(RBracket)
		GameKeyToString(SemiColon)
		GameKeyToString(Comma)
		GameKeyToString(Period)
		GameKeyToString(Quote)
		GameKeyToString(Slash)
		GameKeyToString(BackSlash)
		GameKeyToString(Tilde)
		GameKeyToString(Equal)
		GameKeyToString(Dash)
		GameKeyToString(Space)
		GameKeyToString(Return)
		GameKeyToString(BackSpace)
		GameKeyToString(Tab)
		GameKeyToString(PageUp)
		GameKeyToString(PageDown)
		GameKeyToString(End)
		GameKeyToString(Home)
		GameKeyToString(Insert)
		GameKeyToString(Delete)
		GameKeyToString(Add)
		GameKeyToString(Subtract)
		GameKeyToString(Multiply)
		GameKeyToString(Divide)
		GameKeyToString(Left)
		GameKeyToString(Right)
		GameKeyToString(Up)
		GameKeyToString(Down)
		GameKeyToString(Numpad0)
		GameKeyToString(Numpad1)
		GameKeyToString(Numpad2)
		GameKeyToString(Numpad3)
		GameKeyToString(Numpad4)
		GameKeyToString(Numpad5)
		GameKeyToString(Numpad6)
		GameKeyToString(Numpad7)
		GameKeyToString(Numpad8)
		GameKeyToString(Numpad9)
		GameKeyToString(F1)
		GameKeyToString(F2)
		GameKeyToString(F3)
		GameKeyToString(F4)
		GameKeyToString(F5)
		GameKeyToString(F6)
		GameKeyToString(F7)
		GameKeyToString(F8)
		GameKeyToString(F9)
		GameKeyToString(F10)
		GameKeyToString(F11)
		GameKeyToString(F12)
		GameKeyToString(F13)
		GameKeyToString(F14)
		GameKeyToString(F15)
		GameKeyToString(Pause)
	}

	return "";
}

void CenterObjectOrigin(sf::Sprite& sprite)
{
	sf::FloatRect bounds = sprite.getLocalBounds();
	sprite.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
}

void CenterObjectOrigin(sf::Text& text)
{
	sf::FloatRect bounds = text.getLocalBounds();
	text.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
}

void CenterObjectOrigin(Animation& animation)
{
	sf::FloatRect bounds = animation.getLocalBounds();
	animation.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
}

float toDegree(float radian)
{
	return 180.f / 3.141592653589793238462643383f * radian;
}

float toRadian(float degree)
{
	return 3.141592653589793238462643383f / 180.f * degree;
}

int randomInt(int exclusiveMax)
{
	std::uniform_int_distribution<> distr(0, exclusiveMax - 1);
	return distr(RandomEngine);
}

float length(sf::Vector2f vector)
{
	return std::sqrt(vector.x * vector.x + vector.y * vector.y);
}

sf::Vector2f unitVector(sf::Vector2f vector)
{
	assert(vector != sf::Vector2f(0.f, 0.f));
	return vector / length(vector);
}