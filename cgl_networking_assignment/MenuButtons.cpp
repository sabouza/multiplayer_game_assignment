#include <hppFiles/MenuButtons.hpp>
#include <hppFiles/Utility.hpp>
#include <hppFiles/ResourceContainer.hpp>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>


namespace GUI
{

MenuButtons::MenuButtons(State::Context context)
: buttonCallback()
, Sprite(context.textures->get(Textures::Buttons))
, Text("", context.fonts->get(Fonts::Bebas), 16)
, isToggle(false)
{
	SetTexture(deselected);
	sf::FloatRect bounds = Sprite.getLocalBounds();
	Text.setPosition(bounds.width / 2.f, 63 / 2.f); //63px is the button texture height
}

void MenuButtons::SetCallback(Callback callback)
{
	buttonCallback = std::move(callback);
}

void MenuButtons::SetText(const std::string& text)
{
	Text.setString(text);
	CenterObjectOrigin(Text);
}

void MenuButtons::SetToggle(bool flag)
{
	isToggle = flag;
}

bool MenuButtons::isSelectable() const
{
    return true;
}

void MenuButtons::Select()
{
	Component::Select();

	SetTexture(selected);
}

void MenuButtons::Deselect()
{
	Component::Deselect();

	SetTexture(deselected);
}

void MenuButtons::Activate()
{
	Component::Activate();

	if (buttonCallback)
		buttonCallback();

    // If we are not a toggle then Deactivate the button since we are just momentarily activated.
	if (!isToggle)
		Deactivate();
}

void MenuButtons::Deactivate()
{
	Component::Deactivate();

	if (isToggle)
	{
        // Reset texture to right one depending on if we are selected or not.
		if (isSelected())
			SetTexture(selected);
		else
			SetTexture(deselected);
	}
}

void MenuButtons::ManageEvent(const sf::Event&)
{
}

void MenuButtons::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(Sprite, states);
	target.draw(Text, states);
}


void MenuButtons::SetTexture(Type buttonType)
{
	sf::IntRect textureRect(0,70*buttonType, 210, 70);
	Sprite.setTextureRect(textureRect);
}

}
